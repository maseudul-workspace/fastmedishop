package com.webinfotech.fastmedishop.domain.interactors;

public interface RegisterCspInteractor {
    interface Callback {
        void onRegisterCspSuccess();
        void onRegisterCspFail(String errorMsg);
    }
}
