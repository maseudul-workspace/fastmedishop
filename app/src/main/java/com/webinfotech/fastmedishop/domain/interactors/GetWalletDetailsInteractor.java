package com.webinfotech.fastmedishop.domain.interactors;


import com.webinfotech.fastmedishop.domain.models.WalletDetails;

public interface GetWalletDetailsInteractor {
    interface Callback {
        void onGettingCreditHistorySuccess(WalletDetails walletDetails);
        void onGettingCreditHistoryFail(String errorMsg, int loginError);
    }
}
