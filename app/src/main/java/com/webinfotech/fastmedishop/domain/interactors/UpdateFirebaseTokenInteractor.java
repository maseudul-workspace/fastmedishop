package com.webinfotech.fastmedishop.domain.interactors;

public interface UpdateFirebaseTokenInteractor {
    interface Callback {
        void onFirebaseTokenUpdateSuccess();
        void onFirebaseTokenUpdateFail(String errorMsg);
    }
}
