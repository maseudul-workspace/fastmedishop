package com.webinfotech.fastmedishop.domain.interactors;

import com.webinfotech.fastmedishop.domain.models.UserInfo;

public interface CheckLoginInteractor {
    interface Callback {
        void onLoginSuccess(UserInfo userInfo);
        void onLoginFail(String errorMsg);
    }
}
