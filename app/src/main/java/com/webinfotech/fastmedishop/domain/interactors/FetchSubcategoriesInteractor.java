package com.webinfotech.fastmedishop.domain.interactors;

import com.webinfotech.fastmedishop.domain.models.Subcategory;

public interface FetchSubcategoriesInteractor {
    interface Callback {
        void onGettingSubcategoriesSuccess(Subcategory[] subcategories);
        void onGettingSubcategoriesFail(String errorMsg);
    }
}
