package com.webinfotech.fastmedishop.domain.interactors;

import com.webinfotech.fastmedishop.domain.models.ProductDetailsData;

public interface FetchProductDetailsInteractor {
    interface Callback {
        void onGettingProductDetailsSuccess(ProductDetailsData productDetailsData);
        void onGettingProductDetailsFail(String errorMsg);
    }
}
