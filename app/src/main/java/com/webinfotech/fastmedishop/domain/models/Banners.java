package com.webinfotech.fastmedishop.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Banners {

    @SerializedName("slider1")
    @Expose
    public Slider[] sliders1;

    @SerializedName("slider2")
    @Expose
    public Slider[] sliders2;

    @SerializedName("banners")
    @Expose
    public Slider[] banners;

}
