package com.webinfotech.fastmedishop.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Charges {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("type")
    @Expose
    public int type;

    @SerializedName("description")
    @Expose
    public String description;

    @SerializedName("from_amount")
    @Expose
    public double fromAmount;

    @SerializedName("to_amount")
    @Expose
    public double toAmount;

    @SerializedName("charge")
    @Expose
    public double charge;

}
