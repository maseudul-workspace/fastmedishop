package com.webinfotech.fastmedishop.domain.interactors;

import com.webinfotech.fastmedishop.domain.models.OrderPlaceData;

public interface PayFromOrderHistoryInteractor {
    interface Callback {
        void onPlaceOrderSuccess(OrderPlaceData orderPlaceData);
        void onPlaceOrderFail(String errorMsg, int loginError);
    }
}
