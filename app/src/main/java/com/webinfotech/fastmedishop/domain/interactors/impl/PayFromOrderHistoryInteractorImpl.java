package com.webinfotech.fastmedishop.domain.interactors.impl;

import com.webinfotech.fastmedishop.domain.executors.Executor;
import com.webinfotech.fastmedishop.domain.executors.MainThread;
import com.webinfotech.fastmedishop.domain.interactors.PayFromOrderHistoryInteractor;
import com.webinfotech.fastmedishop.domain.interactors.base.AbstractInteractor;
import com.webinfotech.fastmedishop.domain.models.OrderPlaceData;
import com.webinfotech.fastmedishop.domain.models.OrderPlaceDataResponse;
import com.webinfotech.fastmedishop.repository.AppRepositoryImpl;

public class PayFromOrderHistoryInteractorImpl extends AbstractInteractor implements PayFromOrderHistoryInteractor {

    Callback mCallback;
    AppRepositoryImpl mRepository;
    String apiToken;
    int orderId;

    public PayFromOrderHistoryInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, AppRepositoryImpl mRepository, String apiToken, int orderId) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.apiToken = apiToken;
        this.orderId = orderId;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onPlaceOrderFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(OrderPlaceData orderPlaceData){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onPlaceOrderSuccess(orderPlaceData);
            }
        });
    }

    @Override
    public void run() {
        final OrderPlaceDataResponse orderPlaceDataResponse = mRepository.payFromOrderHistory(apiToken, orderId);
        if (orderPlaceDataResponse == null) {
            notifyError("Something went wrong", 0);
        } else if (!orderPlaceDataResponse.status) {
            notifyError(orderPlaceDataResponse.message, orderPlaceDataResponse.login_error);
        } else {
            postMessage(orderPlaceDataResponse.orderPlaceData);
        }
    }
}
