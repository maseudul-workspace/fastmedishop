package com.webinfotech.fastmedishop.domain.models;

public class Labs {

    public int id;
    public String name;
    public String address;
    public String testsDone;
    public String photo;

    public Labs(int id, String name, String address, String testsDone, String photo) {
        this.id = id;
        this.name = name;
        this.address = address;
        this.testsDone = testsDone;
        this.photo = photo;
    }
}
