package com.webinfotech.fastmedishop.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class OrderPlaceData {

    @SerializedName("payment_type")
    @Expose
    public int paymentType;

    @SerializedName("order")
    @Expose
    public Orders order;

    @SerializedName("payment_data")
    @Expose
    public PaymentData paymentData;

}