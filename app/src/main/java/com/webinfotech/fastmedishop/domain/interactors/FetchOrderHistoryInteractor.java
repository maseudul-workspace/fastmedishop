package com.webinfotech.fastmedishop.domain.interactors;

import com.webinfotech.fastmedishop.domain.models.Orders;

public interface FetchOrderHistoryInteractor {
    interface Callback {
        void onGettingOrderHistorySuccess(Orders[] orders, int totalPage);
        void onGettingOrderHistoryFail(String errorMsg, int loginError);
    }
}
