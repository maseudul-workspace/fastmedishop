package com.webinfotech.fastmedishop.domain.models.testing;

import java.util.ArrayList;

public class ItemMain {

    public String itemName;
    public ArrayList<Item> items;

    public ItemMain(String itemName, ArrayList<Item> items) {
        this.itemName = itemName;
        this.items = items;
    }
}
