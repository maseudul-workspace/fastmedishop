package com.webinfotech.fastmedishop.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class HomeData {

    @SerializedName("new_products")
    @Expose
    public Product[] newProducts;

    @SerializedName("popular_products")
    @Expose
    public Product[] popularProducts;

    @SerializedName("banners")
    @Expose
    public Banners banners;

    @SerializedName("category")
    @Expose
    public Category[] categories;

}
