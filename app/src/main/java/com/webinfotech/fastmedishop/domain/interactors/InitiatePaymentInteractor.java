package com.webinfotech.fastmedishop.domain.interactors;


import com.webinfotech.fastmedishop.domain.models.GatewayOrder;

public interface InitiatePaymentInteractor {
    interface Callback {
        void onInitiatePaymentSuccess(GatewayOrder gatewayOrder);
        void onInitiatePaymentFail(String errorMsg);
    }
}
