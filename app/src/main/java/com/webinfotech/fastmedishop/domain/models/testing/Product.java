package com.webinfotech.fastmedishop.domain.models.testing;

public class Product {

    public int id;
    public String name;
    public String mainImage;
    public String description;
    public String pharmacyName;
    public double minPrice;
    public double mrp;

    public Product(int id, String name, String mainImage, String description, String pharmacyName, double minPrice, double mrp) {
        this.id = id;
        this.name = name;
        this.mainImage = mainImage;
        this.description = description;
        this.pharmacyName = pharmacyName;
        this.minPrice = minPrice;
        this.mrp = mrp;
    }

}
