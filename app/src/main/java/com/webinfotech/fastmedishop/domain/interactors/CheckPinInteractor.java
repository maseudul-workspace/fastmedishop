package com.webinfotech.fastmedishop.domain.interactors;

public interface CheckPinInteractor {
    interface Callback {
        void onPinCodeAvailable();
        void onPinCodeNotAvailable(String errorMsg);
    }
}
