package com.webinfotech.fastmedishop.domain.interactors.impl;

import com.webinfotech.fastmedishop.domain.executors.Executor;
import com.webinfotech.fastmedishop.domain.executors.MainThread;
import com.webinfotech.fastmedishop.domain.interactors.FetchProductListInteractor;
import com.webinfotech.fastmedishop.domain.interactors.base.AbstractInteractor;
import com.webinfotech.fastmedishop.domain.models.Product;
import com.webinfotech.fastmedishop.domain.models.ProductListWrapper;
import com.webinfotech.fastmedishop.repository.AppRepositoryImpl;

public class FetchProductListInteractorImpl extends AbstractInteractor implements FetchProductListInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int categoryId;
    int type;
    int page;

    public FetchProductListInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int categoryId, int type, int page) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.categoryId = categoryId;
        this.type = type;
        this.page = page;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingProductListFail(errorMsg);
            }
        });
    }

    private void postMessage(Product[] products, int totalPage){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingProductListSuccess(products, totalPage);
            }
        });
    }

    @Override
    public void run() {
        final ProductListWrapper productListWrapper = mRepository.fetchProductList(categoryId, type, page);
        if (productListWrapper == null) {
            notifyError("Please Check Your Internet Connection");
        } else if (!productListWrapper.status) {
            notifyError(productListWrapper.message);
        } else {
            postMessage(productListWrapper.products, productListWrapper.totalPage);
        }
    }
}
