package com.webinfotech.fastmedishop.domain.interactors;

import com.webinfotech.fastmedishop.domain.models.HomeData;

public interface FetchHomeDataInteractor {
    interface Callback {
        void onGettingHomeDataSuccess(HomeData homeData);
        void onGettingHomeDataFail(String errorMsg);
    }
}
