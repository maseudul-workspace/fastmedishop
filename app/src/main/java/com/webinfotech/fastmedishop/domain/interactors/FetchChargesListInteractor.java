package com.webinfotech.fastmedishop.domain.interactors;


import com.webinfotech.fastmedishop.domain.models.Charges;

public interface FetchChargesListInteractor {
    interface Callback {
        void onChargesListFetchSuccess(Charges[] charges);
        void onChargesListFetchFail();
    }
}
