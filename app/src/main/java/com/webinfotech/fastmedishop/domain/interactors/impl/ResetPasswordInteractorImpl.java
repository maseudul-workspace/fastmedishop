package com.webinfotech.fastmedishop.domain.interactors.impl;

import com.webinfotech.fastmedishop.domain.executors.Executor;
import com.webinfotech.fastmedishop.domain.executors.MainThread;
import com.webinfotech.fastmedishop.domain.interactors.ResetPasswordInteractor;
import com.webinfotech.fastmedishop.domain.interactors.base.AbstractInteractor;
import com.webinfotech.fastmedishop.domain.models.CommonResponse;
import com.webinfotech.fastmedishop.repository.AppRepositoryImpl;

public class ResetPasswordInteractorImpl extends AbstractInteractor implements ResetPasswordInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String mobile;
    String password;

    public ResetPasswordInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String mobile, String password) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.mobile = mobile;
        this.password = password;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onResetPasswordFail(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onResetPasswordSuccess();
            }
        });
    }

    @Override
    public void run() {
        final CommonResponse commonResponse = mRepository.resetPassword(mobile, password);
        if (commonResponse == null) {
            notifyError("Please Check Your Internet Connection");
        } else if (!commonResponse.status) {
            notifyError(commonResponse.message);
        } else {
            postMessage();
        }
    }

}
