package com.webinfotech.fastmedishop.domain.interactors.impl;

import com.webinfotech.fastmedishop.domain.executors.Executor;
import com.webinfotech.fastmedishop.domain.executors.MainThread;
import com.webinfotech.fastmedishop.domain.interactors.FetchFeaturedProductsInteractor;
import com.webinfotech.fastmedishop.domain.interactors.base.AbstractInteractor;
import com.webinfotech.fastmedishop.domain.models.FeaturedProductsWrapper;
import com.webinfotech.fastmedishop.domain.models.Product;
import com.webinfotech.fastmedishop.repository.AppRepositoryImpl;

public class FetchFeaturedProductsInteractorImpl extends AbstractInteractor implements FetchFeaturedProductsInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;

    public FetchFeaturedProductsInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onFeaturedProductsFetchFail(errorMsg);
            }
        });
    }

    private void postMessage(Product[] products){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onFeaturedProductsFetchSuccess(products);
            }
        });
    }

    @Override
    public void run() {
        final FeaturedProductsWrapper featuredProductsWrapper = mRepository.fetchFeaturedProducts();
        if (featuredProductsWrapper == null) {
            notifyError("Please Check Your Internet Connection");
        } else if (!featuredProductsWrapper.status) {
            notifyError(featuredProductsWrapper.message);
        } else {
            postMessage(featuredProductsWrapper.products);
        }
    }
}
