package com.webinfotech.fastmedishop.domain.interactors;

public interface CancelOrderInteractor {
    interface Callback {
        void onOrderCancelSuccess();
        void onOrderCancelFail(String errorMsg, int loginError);
    }
}
