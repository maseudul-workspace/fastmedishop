package com.webinfotech.fastmedishop.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Pharmacy {

    @SerializedName("id")
    @Expose
    public int id;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("owner_name")
    @Expose
    public String owner_name;

    @SerializedName("mobile_no")
    @Expose
    public String mobile_no;

    @SerializedName("image")
    @Expose
    public String image;

    @SerializedName("location_name")
    @Expose
    public String locationName;

    @SerializedName("email")
    @Expose
    public String email;

}
