package com.webinfotech.fastmedishop.domain.interactors;


import com.webinfotech.fastmedishop.domain.models.ShippingAddress;

public interface FetchShippingAddressDetailsInteractor {
    interface Callback {
        void onGettingAddressDetailsSuccess(ShippingAddress shippingAddress);
        void onGettingAddressDetailsFail(String errorMsg, int loginError);
    }
}
