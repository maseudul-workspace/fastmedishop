package com.webinfotech.fastmedishop.domain.interactors;

public interface SendOtpInteractor {
    interface Callback {
        void onSendOtpSuccess(String otp);
        void onSendOtpFail(String errorMsg);
    }
}
