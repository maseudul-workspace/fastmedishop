package com.webinfotech.fastmedishop.domain.models.testing;

public class Item {

    public String itemName;
    public String itemImage;

    public Item(String itemName, String itemImage) {
        this.itemName = itemName;
        this.itemImage = itemImage;
    }
}
