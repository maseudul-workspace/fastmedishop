package com.webinfotech.fastmedishop.domain.interactors;

import com.webinfotech.fastmedishop.domain.models.ShippingAddress;

public interface FetchShippingAddressInteractor {
    interface Callback {
        void onGettingAddressListSucces(ShippingAddress[] shippingAddresses);
        void onGettingAddressListFail(String errorMsg, int loginError);
    }
}
