package com.webinfotech.fastmedishop.domain.interactors;

public interface ResetPasswordInteractor {
    interface Callback {
        void onResetPasswordSuccess();
        void onResetPasswordFail(String errorMsg);
    }
}
