package com.webinfotech.fastmedishop.domain.interactors;

public interface SendFeedbackInteractor {
    interface Callback {
        void onFeedbackSendSuccess();
        void onFeedbackSendFail(String errorMsg);
    }
}
