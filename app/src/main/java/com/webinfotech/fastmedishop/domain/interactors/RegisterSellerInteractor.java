package com.webinfotech.fastmedishop.domain.interactors;

public interface RegisterSellerInteractor {
    interface Callback {
        void onRegisterSellerSuccess();
        void onRegisterSellerFail(String errorMsg);
    }
}
