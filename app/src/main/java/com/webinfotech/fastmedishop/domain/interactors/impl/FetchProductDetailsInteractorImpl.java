package com.webinfotech.fastmedishop.domain.interactors.impl;

import com.webinfotech.fastmedishop.domain.executors.Executor;
import com.webinfotech.fastmedishop.domain.executors.MainThread;
import com.webinfotech.fastmedishop.domain.interactors.FetchProductDetailsInteractor;
import com.webinfotech.fastmedishop.domain.interactors.base.AbstractInteractor;
import com.webinfotech.fastmedishop.domain.models.ProductDetailsData;
import com.webinfotech.fastmedishop.domain.models.ProductDetailsWrapper;
import com.webinfotech.fastmedishop.repository.AppRepositoryImpl;

public class FetchProductDetailsInteractorImpl extends AbstractInteractor implements FetchProductDetailsInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    int productId;

    public FetchProductDetailsInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, int productId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.productId = productId;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingProductDetailsFail(errorMsg);
            }
        });
    }

    private void postMessage(ProductDetailsData productDetailsData){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingProductDetailsSuccess(productDetailsData);
            }
        });
    }

    @Override
    public void run() {
        final ProductDetailsWrapper productDetailsWrapper = mRepository.fetchProductDetails(productId);
        if (productDetailsWrapper == null) {
            notifyError("Please Check Your Internet Connection");
        } else if (!productDetailsWrapper.status) {
            notifyError(productDetailsWrapper.message);
        } else {
            postMessage(productDetailsWrapper.productDetailsData);
        }
    }
}
