package com.webinfotech.fastmedishop.domain.interactors;

public interface ClaimIncentiveInteractor {
    interface Callback {
        void onIncentiveClaimSuccess();
        void onIncentiveClaimFail(String errorMsg);
    }
}
