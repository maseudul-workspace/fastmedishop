package com.webinfotech.fastmedishop.domain.models;

public class Doctors {

    public int id;
    public String name;
    public String degree;
    public String specialisation;
    public String available;
    public String image;

    public Doctors(int id, String name, String degree, String specialisation, String available, String image) {
        this.id = id;
        this.name = name;
        this.degree = degree;
        this.specialisation = specialisation;
        this.available = available;
        this.image = image;
    }
}
