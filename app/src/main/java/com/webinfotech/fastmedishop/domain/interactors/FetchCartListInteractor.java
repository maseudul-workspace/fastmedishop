package com.webinfotech.fastmedishop.domain.interactors;

import com.webinfotech.fastmedishop.domain.models.CartList;

public interface FetchCartListInteractor {
    interface Callback {
        void onGettingCartListSuccess(CartList[] cartLists);
        void onGettingCartListFail(String errorMsg, int loginError);
    }
}
