package com.webinfotech.fastmedishop.domain.interactors.impl;

import com.webinfotech.fastmedishop.domain.executors.Executor;
import com.webinfotech.fastmedishop.domain.executors.MainThread;
import com.webinfotech.fastmedishop.domain.interactors.GetWalletDetailsInteractor;
import com.webinfotech.fastmedishop.domain.interactors.base.AbstractInteractor;
import com.webinfotech.fastmedishop.domain.models.WalletDetails;
import com.webinfotech.fastmedishop.domain.models.WalletDetailsWrapper;
import com.webinfotech.fastmedishop.repository.AppRepositoryImpl;

public class GetWalletDetailsInteractorImpl extends AbstractInteractor implements GetWalletDetailsInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiKey;
    int userId;

    public GetWalletDetailsInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiKey, int userId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiKey = apiKey;
        this.userId = userId;
    }

    private void notifyError(final String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingCreditHistoryFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(final WalletDetails walletDetails, int totalPage){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingCreditHistorySuccess(walletDetails);
            }
        });
    }

    @Override
    public void run() {
        final WalletDetailsWrapper walletDetailsWrapper = mRepository.getWalletDetails(apiKey, userId);
        if(walletDetailsWrapper == null){
            notifyError("Something went wrong !!! Please check your internet", 0);
        }else if(!walletDetailsWrapper.status){
            notifyError(walletDetailsWrapper.message, walletDetailsWrapper.login_error);
        }else{
            postMessage(walletDetailsWrapper.walletDetails, walletDetailsWrapper.totalPage);
        }
    }
}
