package com.webinfotech.fastmedishop.domain.interactors;

import com.webinfotech.fastmedishop.domain.models.Product;

public interface FetchSearchListInteractor {
    interface Callback {
        void onGettingSearchListSuccess(Product[] products, String searchKey);
        void onGettingSearchListFail(String errorMsg);
    }
}
