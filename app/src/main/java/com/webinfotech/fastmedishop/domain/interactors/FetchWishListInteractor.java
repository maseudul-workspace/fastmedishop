package com.webinfotech.fastmedishop.domain.interactors;

import com.webinfotech.fastmedishop.domain.models.WishList;

public interface FetchWishListInteractor {
    interface Callback {
        void onWishListFetchSuccess(WishList[] wishLists);
        void onWishListFetchFail(String errorMsg, int loginError);
    }
}
