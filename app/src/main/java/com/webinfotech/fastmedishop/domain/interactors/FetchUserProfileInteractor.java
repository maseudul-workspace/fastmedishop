package com.webinfotech.fastmedishop.domain.interactors;


import com.webinfotech.fastmedishop.domain.models.UserInfo;

public interface FetchUserProfileInteractor {
    interface Callback {
        void onGettingUserProfileSuccess(UserInfo userInfo);
        void onGettingUserProfileFail(String errorMsg, int loginError);
    }
}
