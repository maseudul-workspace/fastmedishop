package com.webinfotech.fastmedishop.domain.interactors;


import com.webinfotech.fastmedishop.domain.models.PaymentRequestResponse;

public interface RequestPaymentInteractor {
    interface Callback {
        void onRequestPaymentSuccess(PaymentRequestResponse paymentRequestResponse);
        void onRequestPaymentFail(String errorMsg);
    }
}
