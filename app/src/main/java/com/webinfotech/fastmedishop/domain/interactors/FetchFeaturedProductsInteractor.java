package com.webinfotech.fastmedishop.domain.interactors;

import com.webinfotech.fastmedishop.domain.models.Product;

public interface FetchFeaturedProductsInteractor {
    interface Callback {
        void onFeaturedProductsFetchSuccess(Product[] products);
        void onFeaturedProductsFetchFail(String errorMsg);
    }
}
