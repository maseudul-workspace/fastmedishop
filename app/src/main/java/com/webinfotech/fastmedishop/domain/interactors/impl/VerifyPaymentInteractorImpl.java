package com.webinfotech.fastmedishop.domain.interactors.impl;

import com.webinfotech.fastmedishop.domain.executors.Executor;
import com.webinfotech.fastmedishop.domain.executors.MainThread;
import com.webinfotech.fastmedishop.domain.interactors.VerifyPaymentInteractor;
import com.webinfotech.fastmedishop.domain.interactors.base.AbstractInteractor;
import com.webinfotech.fastmedishop.domain.models.CommonResponse;
import com.webinfotech.fastmedishop.repository.AppRepositoryImpl;

public class VerifyPaymentInteractorImpl extends AbstractInteractor implements VerifyPaymentInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int userId;
    int orderId;
    String instamojoOrderId;

    public VerifyPaymentInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int userId, int orderId, String instamojoOrderId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.userId = userId;
        this.orderId = orderId;
        this.instamojoOrderId = instamojoOrderId;
    }

    private void notifyError() {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onVerifyPaymentFail();
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onVerifyPaymentSuccess();
            }
        });
    }

    @Override
    public void run() {
        CommonResponse commonResponse = mRepository.verifyPayment(apiToken, userId, orderId, instamojoOrderId);
        if (commonResponse == null) {
            mCallback.onVerifyPaymentFail();
        } else if (!commonResponse.status) {
            mCallback.onVerifyPaymentFail();
        } else {
            mCallback.onVerifyPaymentSuccess();
        }
    }



}
