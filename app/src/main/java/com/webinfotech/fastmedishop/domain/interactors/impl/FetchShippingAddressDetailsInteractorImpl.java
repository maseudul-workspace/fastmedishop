package com.webinfotech.fastmedishop.domain.interactors.impl;

import com.webinfotech.fastmedishop.domain.executors.Executor;
import com.webinfotech.fastmedishop.domain.executors.MainThread;
import com.webinfotech.fastmedishop.domain.interactors.FetchShippingAddressDetailsInteractor;
import com.webinfotech.fastmedishop.domain.interactors.base.AbstractInteractor;
import com.webinfotech.fastmedishop.domain.models.ShippingAddress;
import com.webinfotech.fastmedishop.domain.models.ShippingAddressDetailsWrapper;
import com.webinfotech.fastmedishop.repository.AppRepositoryImpl;

public class FetchShippingAddressDetailsInteractorImpl extends AbstractInteractor implements FetchShippingAddressDetailsInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int userId;
    int addressId;

    public FetchShippingAddressDetailsInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int userId, int addressId) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.userId = userId;
        this.addressId = addressId;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingAddressDetailsFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(ShippingAddress shippingAddresses){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingAddressDetailsSuccess(shippingAddresses);
            }
        });
    }

    @Override
    public void run() {
        ShippingAddressDetailsWrapper shippingAddressDetailsWrapper = mRepository.fetchShippingAddressDetails(apiToken, userId, addressId);
        if (shippingAddressDetailsWrapper == null) {
            notifyError("Slow Internet Connection", shippingAddressDetailsWrapper.login_error);
        } else if (!shippingAddressDetailsWrapper.status) {
            notifyError(shippingAddressDetailsWrapper.message, shippingAddressDetailsWrapper.login_error);
        } else {
            postMessage(shippingAddressDetailsWrapper.shippingAddresses);
        }
    }
}
