package com.webinfotech.fastmedishop.domain.interactors.impl;

import com.webinfotech.fastmedishop.domain.executors.Executor;
import com.webinfotech.fastmedishop.domain.executors.MainThread;
import com.webinfotech.fastmedishop.domain.interactors.RegisterSellerInteractor;
import com.webinfotech.fastmedishop.domain.interactors.base.AbstractInteractor;
import com.webinfotech.fastmedishop.domain.models.CommonResponse;
import com.webinfotech.fastmedishop.repository.AppRepositoryImpl;

public class RegisterSellerinteractorImpl extends AbstractInteractor implements RegisterSellerInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String name;
    String dl_no;
    String gst_no;
    String owner_name;
    String owner_no;
    String whatsapp_No;
    String phone_pay_no;
    String email;
    String pin;
    String address;
    String bank_no;
    String ifsc;

    public RegisterSellerinteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String name, String dl_no, String gst_no, String owner_name, String owner_no, String whatsapp_No, String phone_pay_no, String email, String pin, String address, String bank_no, String ifsc) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.name = name;
        this.dl_no = dl_no;
        this.gst_no = gst_no;
        this.owner_name = owner_name;
        this.owner_no = owner_no;
        this.whatsapp_No = whatsapp_No;
        this.phone_pay_no = phone_pay_no;
        this.email = email;
        this.pin = pin;
        this.address = address;
        this.bank_no = bank_no;
        this.ifsc = ifsc;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onRegisterSellerFail(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onRegisterSellerSuccess();
            }
        });
    }

    @Override
    public void run() {
        final CommonResponse commonResponse = mRepository.registerSeller(name, dl_no, gst_no, owner_name, owner_no, whatsapp_No, phone_pay_no, email, pin, address, bank_no, ifsc);
        if (commonResponse == null) {
            notifyError("Slow Internet Connection");
        } else if (!commonResponse.status) {
            notifyError(commonResponse.message);
        } else {
            postMessage();
        }
    }
}
