package com.webinfotech.fastmedishop.domain.interactors.impl;

import com.webinfotech.fastmedishop.domain.executors.Executor;
import com.webinfotech.fastmedishop.domain.executors.MainThread;
import com.webinfotech.fastmedishop.domain.interactors.FetchHomeDataInteractor;
import com.webinfotech.fastmedishop.domain.interactors.base.AbstractInteractor;
import com.webinfotech.fastmedishop.domain.models.HomeData;
import com.webinfotech.fastmedishop.domain.models.HomeDataWrapper;
import com.webinfotech.fastmedishop.repository.AppRepositoryImpl;

public class FetchHomeDataInteractorImpl extends AbstractInteractor implements FetchHomeDataInteractor {

    Callback mCallback;
    AppRepositoryImpl mRepository;

    public FetchHomeDataInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, AppRepositoryImpl mRepository) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingHomeDataFail(errorMsg);
            }
        });
    }

    private void postMessage(HomeData homeData){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingHomeDataSuccess(homeData);
            }
        });
    }

    @Override
    public void run() {
        final HomeDataWrapper homeDataWrapper = mRepository.fetchHomeData();
        if (homeDataWrapper == null) {
            notifyError("Please Check Your Internet Connection");
        } else if (!homeDataWrapper.status) {
            notifyError(homeDataWrapper.message);
        } else {
            postMessage(homeDataWrapper.homeData);
        }
    }
}
