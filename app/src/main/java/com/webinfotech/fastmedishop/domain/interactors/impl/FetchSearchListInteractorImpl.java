package com.webinfotech.fastmedishop.domain.interactors.impl;


import com.webinfotech.fastmedishop.domain.executors.Executor;
import com.webinfotech.fastmedishop.domain.executors.MainThread;
import com.webinfotech.fastmedishop.domain.interactors.FetchSearchListInteractor;
import com.webinfotech.fastmedishop.domain.interactors.base.AbstractInteractor;
import com.webinfotech.fastmedishop.domain.models.Product;
import com.webinfotech.fastmedishop.domain.models.SearchListWrapper;
import com.webinfotech.fastmedishop.repository.AppRepositoryImpl;

public class FetchSearchListInteractorImpl extends AbstractInteractor implements FetchSearchListInteractor {

    Callback mCallback;
    AppRepositoryImpl mRepository;
    String searchKey;

    public FetchSearchListInteractorImpl(Executor threadExecutor, MainThread mainThread, Callback mCallback, AppRepositoryImpl mRepository, String searchKey) {
        super(threadExecutor, mainThread);
        this.mCallback = mCallback;
        this.mRepository = mRepository;
        this.searchKey = searchKey;
    }

    private void notifyError(final String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingSearchListFail(errorMsg);
            }
        });
    }

    private void postMessage(Product[] products, String searchKey){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingSearchListSuccess(products, searchKey);
            }
        });
    }

    @Override
    public void run() {
        final SearchListWrapper searchListWrapper = mRepository.fetchSearchList(searchKey);
        if (searchListWrapper == null) {
            notifyError("Please Check Your Internet Connection");
        } else if (!searchListWrapper.status) {
            notifyError(searchListWrapper.message);
        } else {
            postMessage(searchListWrapper.searchData.products, searchListWrapper.searchData.searchKey);
        }
    }
}
