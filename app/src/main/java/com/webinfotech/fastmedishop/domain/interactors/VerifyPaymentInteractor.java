package com.webinfotech.fastmedishop.domain.interactors;

public interface VerifyPaymentInteractor {
    interface Callback {
        void onVerifyPaymentSuccess();
        void onVerifyPaymentFail();
    }
}
