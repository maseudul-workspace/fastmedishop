package com.webinfotech.fastmedishop.domain.interactors;

import com.webinfotech.fastmedishop.domain.models.Pharmacy;

public interface FetchPharmacyListInteractor {
    interface Callback {
        void onGettingPharmacyListSuccess(Pharmacy[] pharmacies);
        void onGettingPharmacyListFail(String errorMsg);
    }
}
