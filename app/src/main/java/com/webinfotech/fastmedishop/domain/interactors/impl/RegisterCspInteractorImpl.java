package com.webinfotech.fastmedishop.domain.interactors.impl;

import com.webinfotech.fastmedishop.domain.executors.Executor;
import com.webinfotech.fastmedishop.domain.executors.MainThread;
import com.webinfotech.fastmedishop.domain.interactors.RegisterCspInteractor;
import com.webinfotech.fastmedishop.domain.interactors.base.AbstractInteractor;
import com.webinfotech.fastmedishop.domain.models.CommonResponse;
import com.webinfotech.fastmedishop.repository.AppRepositoryImpl;

public class RegisterCspInteractorImpl extends AbstractInteractor implements RegisterCspInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String name;
    String pharmacyName;
    String phoneNo;
    String phonePayNo;
    String pin;
    String address;
    String bank;
    String ifsc;
    String email;

    public RegisterCspInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String name, String pharmacyName, String phoneNo, String phonePayNo, String pin, String address, String bank, String ifsc, String email) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.name = name;
        this.pharmacyName = pharmacyName;
        this.phoneNo = phoneNo;
        this.phonePayNo = phonePayNo;
        this.pin = pin;
        this.address = address;
        this.bank = bank;
        this.ifsc = ifsc;
        this.email = email;
    }

    private void notifyError(String errorMsg) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onRegisterCspFail(errorMsg);
            }
        });
    }

    private void postMessage(){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onRegisterCspSuccess();
            }
        });
    }

    @Override
    public void run() {
        final CommonResponse commonResponse = mRepository.registerCsp(name, pharmacyName, this.phoneNo, phonePayNo, pin, address, bank, ifsc, email);
        if (commonResponse == null) {
            notifyError("Slow Internet Connection");
        } else if (!commonResponse.status) {
            notifyError(commonResponse.message);
        } else {
            postMessage();
        }
    }
}
