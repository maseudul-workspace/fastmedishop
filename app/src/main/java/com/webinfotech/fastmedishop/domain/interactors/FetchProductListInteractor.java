package com.webinfotech.fastmedishop.domain.interactors;


import com.webinfotech.fastmedishop.domain.models.Product;

public interface FetchProductListInteractor {
    interface Callback {
        void onGettingProductListSuccess(Product[] products, int totalPage);
        void onGettingProductListFail(String errorMsg);
    }
}
