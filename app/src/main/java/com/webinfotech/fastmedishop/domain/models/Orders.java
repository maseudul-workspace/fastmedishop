package com.webinfotech.fastmedishop.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Orders {

    @SerializedName("user_id")
    @Expose
    public int userId;

    @SerializedName("shipping_address_id")
    @Expose
    public int shipping_address_id;

    @SerializedName("payment_type")
    @Expose
    public int payment_type;

    @SerializedName("payment_status")
    @Expose
    public int payment_status;

    @SerializedName("is_prescription")
    @Expose
    public int is_prescription;

    @SerializedName("cod_charge")
    @Expose
    public String cod_charge;

    @SerializedName("shipping_charge")
    @Expose
    public String shipping_charge;

    @SerializedName("wallet_pay")
    @Expose
    public double walletPay;

    @SerializedName("total_amount")
    @Expose
    public String totalAmount;

    @SerializedName("payable_amount")
    @Expose
    public String payableAmount;

    @SerializedName("discount")
    @Expose
    public double discount;

    @SerializedName("order_type")
    @Expose
    public int order_type;

    @SerializedName("order_prescription_required")
    @Expose
    public int order_prescription_required;

    @SerializedName("seller_pharmacy_name")
    @Expose
    public String seller_pharmacy_name;

    @SerializedName("seller_pharmacy_id")
    @Expose
    public int seller_pharmacy_id;

    @SerializedName("created_at")
    @Expose
    public String createdAt;

    //    Order Id and Id are same
    @SerializedName("id")
    @Expose
    public int id;

    //    Order Id and Id are same
    @SerializedName("order_id")
    @Expose
    public int orderId;

    @SerializedName("payment_request_id")
    @Expose
    public String paymentRequestId;

    @SerializedName("payment_id")
    @Expose
    public String paymentId;

    @SerializedName("remarks")
    @Expose
    public String remarks;

    @SerializedName("dispatch_remarks")
    @Expose
    public String dispatch_remarks;

    @SerializedName("delay_remarks")
    @Expose
    public String delay_remarks;

    @SerializedName("delivery_status")
    @Expose
    public int deliveryStatus;

    @SerializedName("date")
    @Expose
    public String date;

    @SerializedName("prescription")
    @Expose
    public String prescription;

    @SerializedName("items")
    @Expose
    public OrderProducts[] orderProducts;

}
