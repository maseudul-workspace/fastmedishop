package com.webinfotech.fastmedishop.domain.interactors.impl;

import com.webinfotech.fastmedishop.domain.executors.Executor;
import com.webinfotech.fastmedishop.domain.executors.MainThread;
import com.webinfotech.fastmedishop.domain.interactors.FetchOrderHistoryInteractor;
import com.webinfotech.fastmedishop.domain.interactors.base.AbstractInteractor;
import com.webinfotech.fastmedishop.domain.models.OrderHistoryWrapper;
import com.webinfotech.fastmedishop.domain.models.Orders;
import com.webinfotech.fastmedishop.repository.AppRepositoryImpl;

public class FetchOrderHistoryInteractorImpl extends AbstractInteractor implements FetchOrderHistoryInteractor {

    AppRepositoryImpl mRepository;
    Callback mCallback;
    String apiToken;
    int userId;
    int page;

    public FetchOrderHistoryInteractorImpl(Executor threadExecutor, MainThread mainThread, AppRepositoryImpl mRepository, Callback mCallback, String apiToken, int userId, int page) {
        super(threadExecutor, mainThread);
        this.mRepository = mRepository;
        this.mCallback = mCallback;
        this.apiToken = apiToken;
        this.userId = userId;
        this.page = page;
    }

    private void notifyError(String errorMsg, int loginError) {
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingOrderHistoryFail(errorMsg, loginError);
            }
        });
    }

    private void postMessage(Orders[] orders, int totalPage){
        mMainThread.post(new Runnable() {
            @Override
            public void run() {
                mCallback.onGettingOrderHistorySuccess(orders, totalPage);
            }
        });
    }

    @Override
    public void run() {
        final OrderHistoryWrapper orderHistoryWrapper = mRepository.fetchOrderList(apiToken, userId, page);
        if (orderHistoryWrapper == null) {
            notifyError("Please Check Your Internet Connection", 0);
        } else if (!orderHistoryWrapper.status) {
            notifyError(orderHistoryWrapper.message, orderHistoryWrapper.login_error);
        } else {
            postMessage(orderHistoryWrapper.orders, orderHistoryWrapper.total_page);
        }
    }
}
