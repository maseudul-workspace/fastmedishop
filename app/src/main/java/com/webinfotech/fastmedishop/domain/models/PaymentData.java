package com.webinfotech.fastmedishop.domain.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PaymentData {

    @SerializedName("access_token")
    @Expose
    public AccessToken accessToken;

    @SerializedName("amount")
    @Expose
    public double amount;

    @SerializedName("order_id")
    @Expose
    public String orderId;

    @SerializedName("name")
    @Expose
    public String name;

    @SerializedName("email")
    @Expose
    public String email;

    @SerializedName("mobile")
    @Expose
    public String mobile;

}
