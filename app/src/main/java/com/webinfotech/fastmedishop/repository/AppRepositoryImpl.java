package com.webinfotech.fastmedishop.repository;

import android.util.Log;

import com.google.gson.Gson;
import com.webinfotech.fastmedishop.domain.models.CartListWrapper;
import com.webinfotech.fastmedishop.domain.models.ChargesListWrapper;
import com.webinfotech.fastmedishop.domain.models.CommonResponse;
import com.webinfotech.fastmedishop.domain.models.FeaturedProductsWrapper;
import com.webinfotech.fastmedishop.domain.models.HomeDataWrapper;
import com.webinfotech.fastmedishop.domain.models.OrderHistoryWrapper;
import com.webinfotech.fastmedishop.domain.models.OrderPlaceDataResponse;
import com.webinfotech.fastmedishop.domain.models.OtpResponse;
import com.webinfotech.fastmedishop.domain.models.PharmacyListWrapper;
import com.webinfotech.fastmedishop.domain.models.ProductDetailsWrapper;
import com.webinfotech.fastmedishop.domain.models.ProductListWrapper;
import com.webinfotech.fastmedishop.domain.models.SearchListWrapper;
import com.webinfotech.fastmedishop.domain.models.ShippingAddressDetailsWrapper;
import com.webinfotech.fastmedishop.domain.models.ShippingAddressWrapper;
import com.webinfotech.fastmedishop.domain.models.SubcategoryWrapper;
import com.webinfotech.fastmedishop.domain.models.UserInfoWrapper;
import com.webinfotech.fastmedishop.domain.models.WalletDetailsWrapper;
import com.webinfotech.fastmedishop.domain.models.WishlistWrapper;

import java.io.File;

import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Response;

public class AppRepositoryImpl {

    AppRepository mRepository;

    public AppRepositoryImpl() {
        mRepository = ApiClient.createService(AppRepository.class);
    }

    public HomeDataWrapper fetchHomeData() {
        HomeDataWrapper homeDataWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchHomeData();
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    homeDataWrapper = null;
                }else{
                    homeDataWrapper = gson.fromJson(responseBody, HomeDataWrapper.class);
                }
            } else {
                homeDataWrapper = null;
            }
        }catch (Exception e){
            homeDataWrapper = null;
        }
        return homeDataWrapper;
    }

    public SubcategoryWrapper fetchSubcategories(int categoryId) {
        SubcategoryWrapper subcategoryWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchSubcategories(categoryId);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    subcategoryWrapper = null;
                }else{
                    subcategoryWrapper = gson.fromJson(responseBody, SubcategoryWrapper.class);
                }
            } else {
                subcategoryWrapper = null;
            }
        }catch (Exception e){
            subcategoryWrapper = null;
        }
        return subcategoryWrapper;
    }

    public ProductListWrapper fetchProductList(int categoryId, int type, int page) {
        ProductListWrapper productListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchProductList(categoryId, type, page);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    productListWrapper = null;
                }else{
                    productListWrapper = gson.fromJson(responseBody, ProductListWrapper.class);
                }
            } else {
                productListWrapper = null;
            }
        }catch (Exception e){
            productListWrapper = null;
        }
        return productListWrapper;
    }

    public ProductDetailsWrapper fetchProductDetails(int productId) {
        ProductDetailsWrapper productDetailsWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchProductDetails(productId);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    productDetailsWrapper = null;
                }else{
                    productDetailsWrapper = gson.fromJson(responseBody, ProductDetailsWrapper.class);
                }
            } else {
                productDetailsWrapper = null;
            }
        }catch (Exception e){
            productDetailsWrapper = null;
        }
        return productDetailsWrapper;
    }

    public CommonResponse registerUser(String name,
                                       String email,
                                       String mobile,
                                       String state,
                                       String city,
                                       String pin,
                                       String address,
                                       String password,
                                       String confirmPassword
    ) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> update = mRepository.registerUser(name, email, mobile, state, city, pin, address, password, confirmPassword);
            Response<ResponseBody> response = update.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public OtpResponse sendOtp(String phone) {
        OtpResponse otpResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> send = mRepository.sendOtp(phone);
            Response<ResponseBody> response = send.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    otpResponse = null;
                }else{
                    otpResponse = gson.fromJson(responseBody, OtpResponse.class);
                }
            } else {
                otpResponse = null;
            }
        }catch (Exception e){
            otpResponse = null;
        }
        return otpResponse;
    }

    public UserInfoWrapper checkLogin(String phone, String password){
        UserInfoWrapper userInfoWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> check = mRepository.checkLogin(phone, password);

            Response<ResponseBody> response = check.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    userInfoWrapper = null;
                }else{
                    userInfoWrapper = gson.fromJson(responseBody, UserInfoWrapper.class);
                }
            } else {
                userInfoWrapper = null;
            }
        }catch (Exception e){
            userInfoWrapper = null;
        }
        return userInfoWrapper;
    }

    public ShippingAddressWrapper fetchShippingAddress(String apiToken, int userId){
        ShippingAddressWrapper shippingAddressWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchShippingAddressList("Bearer " + apiToken, userId);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    shippingAddressWrapper = null;
                }else{
                    shippingAddressWrapper = gson.fromJson(responseBody, ShippingAddressWrapper.class);
                }
            } else {
                shippingAddressWrapper = null;
            }
        }catch (Exception e){
            shippingAddressWrapper = null;
        }
        return shippingAddressWrapper;
    }

    public ShippingAddressDetailsWrapper fetchShippingAddressDetails(String apiToken, int userId, int addressId){
        ShippingAddressDetailsWrapper shippingAddressWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchShippingAddressDetails("Bearer " + apiToken, userId, addressId);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    shippingAddressWrapper = null;
                }else{
                    shippingAddressWrapper = gson.fromJson(responseBody, ShippingAddressDetailsWrapper.class);
                }
            } else {
                shippingAddressWrapper = null;
            }
        }catch (Exception e){
            shippingAddressWrapper = null;
        }
        return shippingAddressWrapper;
    }

    public CommonResponse addShippingAddress(  String apiToken,
                                               int userId,
                                               String name,
                                               String email,
                                               String mobile,
                                               String state,
                                               String city,
                                               String pin,
                                               String address
    ) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> add = mRepository.addShippingAddress("Bearer " + apiToken, userId, name, email, mobile, state, city, pin, address);
            Response<ResponseBody> response = add.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse updateShippingAddress(  String apiToken,
                                                  int userId,
                                                  int addressId,
                                                  String name,
                                                  String email,
                                                  String mobile,
                                                  String state,
                                                  String city,
                                                  String pin,
                                                  String address
    ) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> update = mRepository.updateShippingAddress("Bearer " + apiToken, userId, addressId, name, email, mobile, state, city, pin, address);
            Response<ResponseBody> response = update.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse deleteShippingAddress(String apiToken, int addressId){
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> delete = mRepository.deleteShippingAddress("Bearer " + apiToken, addressId);
            Response<ResponseBody> response = delete.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public UserInfoWrapper fetchUserProfile(String apiToken, int userId){
        UserInfoWrapper userInfoWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchUserProfile("Bearer " + apiToken, userId);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    userInfoWrapper = null;
                }else{
                    userInfoWrapper = gson.fromJson(responseBody, UserInfoWrapper.class);
                }
            } else {
                userInfoWrapper = null;
            }
        }catch (Exception e){
            userInfoWrapper = null;
        }
        return userInfoWrapper;
    }

    public CommonResponse updateUser(  String apiToken,
                                       int userId,
                                       String name,
                                       String email,
                                       String mobile,
                                       String state,
                                       String city,
                                       String pin,
                                       String address
    ) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> update = mRepository.updateUser("Bearer " + apiToken, userId, name, email, mobile, state, city, pin, address);
            Response<ResponseBody> response = update.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse changePassword(String apiToken, int userId, String currentPassword, String newPassword){
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> change = mRepository.changePassword("Bearer " + apiToken, userId, currentPassword, newPassword, newPassword);
            Response<ResponseBody> response = change.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public WishlistWrapper fetchWishList(String apiToken, int userId) {
        WishlistWrapper wishlistWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchWishList("Bearer " + apiToken, userId);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    wishlistWrapper = null;
                }else{
                    wishlistWrapper = gson.fromJson(responseBody, WishlistWrapper.class);
                }
            } else {
                wishlistWrapper = null;
            }
        }catch (Exception e){
            wishlistWrapper = null;
        }
        return wishlistWrapper;
    }

    public CommonResponse addToWishList(String apiToken, int userId, int productId) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> add = mRepository.addToWishList("Bearer " + apiToken, userId, productId);
            Response<ResponseBody> response = add.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse removeFromWishlist(String apiToken, int wishListId) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> remove = mRepository.removeFromWishList("Bearer " + apiToken, wishListId);
            Response<ResponseBody> response = remove.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse addToCart(String apiToken, int userId, int productId, String qty, String sizeId){
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> add = mRepository.addToCart("Bearer " + apiToken, userId, productId, qty, sizeId);
            Response<ResponseBody> response = add.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CartListWrapper fetchCartList(String apiToken, int userId) {
        CartListWrapper cartListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchCartList("Bearer " + apiToken, userId);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    cartListWrapper = null;
                }else{
                    cartListWrapper = gson.fromJson(responseBody, CartListWrapper.class);
                }
            } else {
                cartListWrapper = null;
            }
        }catch (Exception e){
            cartListWrapper = null;
        }
        return cartListWrapper;
    }

    public CommonResponse updateCart(String apiToken, int cartId, int quantity) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> update = mRepository.updateCart("Bearer " + apiToken, cartId, quantity);
            Response<ResponseBody> response = update.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse removeCartItem(String apiToken, int cartId) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> remove = mRepository.deleteCart("Bearer " + apiToken, cartId);
            Response<ResponseBody> response = remove.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public ChargesListWrapper fetchChargesList() {
        ChargesListWrapper chargesListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchChargesList();
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    chargesListWrapper = null;
                }else{
                    chargesListWrapper = gson.fromJson(responseBody, ChargesListWrapper.class);
                }
            } else {
                chargesListWrapper = null;
            }
        }catch (Exception e){
            chargesListWrapper = null;
        }
        return chargesListWrapper;
    }

    public WalletDetailsWrapper getWalletDetails(String apiKey, int userid){
        WalletDetailsWrapper walletDetailsWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetchHistory = mRepository.fetchWalletDetails("Bearer " + apiKey, userid);
            Response<ResponseBody> response = fetchHistory.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    walletDetailsWrapper = null;
                }else{
                    walletDetailsWrapper = gson.fromJson(responseBody, WalletDetailsWrapper.class);

                }
            } else {
                walletDetailsWrapper = null;
            }
        }catch (Exception e){
            walletDetailsWrapper = null;
        }
        return  walletDetailsWrapper;
    }

    public SearchListWrapper fetchSearchList(String searchKey){
        SearchListWrapper searchListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchSearchList(searchKey);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    searchListWrapper = null;
                }else{
                    searchListWrapper = gson.fromJson(responseBody, SearchListWrapper.class);
                }
            } else {
                searchListWrapper = null;
            }
        }catch (Exception e){
            searchListWrapper = null;
        }
        return  searchListWrapper;
    }

    public OrderPlaceDataResponse placeOrder(String authorization, int userId, int paymentMethod, int addressId, int orderType, int isWallet, int pharmacyId, String pharmacyName, String prescription) {

        RequestBody userIdRequestBody = RequestBody.create(okhttp3.MultipartBody.FORM, Integer.toString(userId));
        RequestBody paymentMethodRequestBody = RequestBody.create(okhttp3.MultipartBody.FORM, Integer.toString(paymentMethod));
        RequestBody addressIdRequestBody = RequestBody.create(okhttp3.MultipartBody.FORM, Integer.toString(addressId));
        RequestBody orderTypeRequestBody = RequestBody.create(okhttp3.MultipartBody.FORM, Integer.toString(orderType));
        RequestBody isWalletRequestBody = RequestBody.create(okhttp3.MultipartBody.FORM, Integer.toString(isWallet));
        RequestBody pharmacyIdRequestBody = null;
        RequestBody pharmacyNameRequestBody = null;
        if (pharmacyId > 0) {
            pharmacyIdRequestBody = RequestBody.create(okhttp3.MultipartBody.FORM, Integer.toString(pharmacyId));
            pharmacyNameRequestBody = RequestBody.create(okhttp3.MultipartBody.FORM, pharmacyName);
        }

        MultipartBody.Part imageFileBody = null;
        if (prescription != null) {
            File imageFile = new File(prescription);
            // create RequestBody instance from file
            RequestBody requestImageFile =
                    RequestBody.create(
                            MediaType.parse("multipart/form-data"),
                            imageFile
                    );

            // MultipartBody.Part is used to send also the actual file name
            imageFileBody = MultipartBody.Part.createFormData("prescription", imageFile.getName(), requestImageFile);
        }

        OrderPlaceDataResponse orderPlaceDataResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> submit = mRepository.placeOrder("Bearer " + authorization, userIdRequestBody, paymentMethodRequestBody, addressIdRequestBody, orderTypeRequestBody, isWalletRequestBody, pharmacyIdRequestBody, pharmacyNameRequestBody, imageFileBody);
            Response<ResponseBody> response = submit.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    orderPlaceDataResponse = null;
                }else{
                    orderPlaceDataResponse = gson.fromJson(responseBody, OrderPlaceDataResponse.class);
                }
            } else {
                orderPlaceDataResponse = null;
            }
        }catch (Exception e){
            orderPlaceDataResponse = null;
        }
        return  orderPlaceDataResponse;
    }

    public OrderHistoryWrapper fetchOrderList(String apiToken, int userId, int page) {
        OrderHistoryWrapper orderHistoryWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchOrderList("Bearer " + apiToken, userId, page);
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    orderHistoryWrapper = null;
                }else{
                    orderHistoryWrapper = gson.fromJson(responseBody, OrderHistoryWrapper.class);
                }
            } else {
                orderHistoryWrapper = null;
            }
        } catch (Exception e){
            orderHistoryWrapper = null;
        }
        return orderHistoryWrapper;
    }

    public CommonResponse setPaymentTransactionId(String apiToken, int orderId, String paymentRequestId) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> request = mRepository.requestPaymentId("Bearer " + apiToken, orderId, paymentRequestId);
            Response<ResponseBody> response = request.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse verifyPayment(String apiToken, int userId, int orderId, String instamozoOrderId) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;

        try {
            Call<ResponseBody> verify = mRepository.verifyPayment("Bearer " + apiToken, userId, orderId, instamozoOrderId);
            Response<ResponseBody> response = verify.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse cancelOrder(String apiToken, int orderId) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> cancel = mRepository.cancelOrder("Bearer " + apiToken, orderId);
            Response<ResponseBody> response = cancel.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse registerCsp(String name,
                                      String pharmacyName,
                                      String phoneNo,
                                      String phonePayNo,
                                      String pin,
                                      String address,
                                      String bank,
                                      String ifsc,
                                      String email) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> register = mRepository.registerCsp(name, pharmacyName, phoneNo, phonePayNo, pin, address, bank, ifsc, email);
            Response<ResponseBody> response = register.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse registerSeller( String name,
                                          String dl_no,
                                          String gst_no,
                                          String owner_name,
                                          String owner_no,
                                          String whatsapp_No,
                                          String phone_pay_no,
                                          String email,
                                          String pin,
                                          String address,
                                          String bank_no,
                                          String ifsc
    ) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> register = mRepository.registerSeller(name, dl_no, gst_no, owner_name, owner_no, whatsapp_No, phone_pay_no, email, pin, address, bank_no, ifsc);
            Response<ResponseBody> response = register.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse claimIncentive(String cspId, String phoneNo, String incentiveBasis) {
        RequestBody cspIdRequestBody = RequestBody.create(okhttp3.MultipartBody.FORM, cspId);
        RequestBody phoneNoRequestBody = RequestBody.create(okhttp3.MultipartBody.FORM, phoneNo);

        MultipartBody.Part imageFileBody = null;
        if (incentiveBasis != null) {
            File imageFile = new File(incentiveBasis);
            // create RequestBody instance from file
            RequestBody requestImageFile =
                    RequestBody.create(
                            MediaType.parse("multipart/form-data"),
                            imageFile
                    );

            // MultipartBody.Part is used to send also the actual file name
            imageFileBody = MultipartBody.Part.createFormData("incentive_basis", imageFile.getName(), requestImageFile);
        }

        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> claim = mRepository.claimIncentive(cspIdRequestBody, phoneNoRequestBody, imageFileBody);
            Response<ResponseBody> response = claim.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return  commonResponse;
    }

    public OrderPlaceDataResponse payFromOrderHistory(String apiToken, int orderId) {
        OrderPlaceDataResponse orderPlaceDataResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> submit = mRepository.payFromOrderHistory("Bearer " + apiToken, orderId);
            Response<ResponseBody> response = submit.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    orderPlaceDataResponse = null;
                }else{
                    orderPlaceDataResponse = gson.fromJson(responseBody, OrderPlaceDataResponse.class);
                }
            } else {
                orderPlaceDataResponse = null;
            }
        }catch (Exception e){
            orderPlaceDataResponse = null;
        }
        return  orderPlaceDataResponse;
    }

    public PharmacyListWrapper fetchPharmacyList() {
        PharmacyListWrapper pharmacyListWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchPharmacyList();
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    pharmacyListWrapper = null;
                }else{
                    pharmacyListWrapper = gson.fromJson(responseBody, PharmacyListWrapper.class);
                }
            } else {
                pharmacyListWrapper = null;
            }
        }catch (Exception e){
            pharmacyListWrapper = null;
        }
        return  pharmacyListWrapper;
    }

    public FeaturedProductsWrapper fetchFeaturedProducts() {
        FeaturedProductsWrapper featuredProductsWrapper;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> fetch = mRepository.fetchFeaturedProducts();
            Response<ResponseBody> response = fetch.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    featuredProductsWrapper = null;
                }else{
                    featuredProductsWrapper = gson.fromJson(responseBody, FeaturedProductsWrapper.class);
                }
            } else {
                featuredProductsWrapper = null;
            }
        }catch (Exception e){
            featuredProductsWrapper = null;
        }
        return  featuredProductsWrapper;
    }

    public OtpResponse sendForgetPasswordOtp(String phone) {
        OtpResponse otpResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> send = mRepository.sendForgetPasswordOtp(phone);
            Response<ResponseBody> response = send.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    otpResponse = null;
                }else{
                    otpResponse = gson.fromJson(responseBody, OtpResponse.class);
                }
            } else {
                otpResponse = null;
            }
        }catch (Exception e){
            otpResponse = null;
        }
        return otpResponse;
    }

    public CommonResponse resetPassword(String mobile, String password){
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> reset = mRepository.resetPassword(mobile, password, password);
            Response<ResponseBody> response = reset.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        } catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse checkPin(String pin){
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> check = mRepository.checkPin(pin);
            Response<ResponseBody> response = check.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        } catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse sendFeedback(String message){
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> send = mRepository.sendFeedback(message);
            Response<ResponseBody> response = send.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        } catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

    public CommonResponse updateFirebaseToken( String authorization,
                                               int userId,
                                               String token
    ) {
        CommonResponse commonResponse;
        String responseBody = "";
        Gson gson = new Gson();
        Boolean isErrorResponse = false;
        try {
            Call<ResponseBody> update = mRepository.updateFirebaseToken("Bearer " + authorization, userId, token);
            Response<ResponseBody> response = update.execute();
            if(response.body() != null){
                responseBody = response.body().string();
            } else if(response.errorBody() != null){
                responseBody = response.errorBody().string();
            }
            if (responseBody != null && !responseBody.isEmpty()) {
                if(isErrorResponse){
                    commonResponse = null;
                }else{
                    commonResponse = gson.fromJson(responseBody, CommonResponse.class);
                }
            } else {
                commonResponse = null;
            }
        }catch (Exception e){
            commonResponse = null;
        }
        return commonResponse;
    }

}
