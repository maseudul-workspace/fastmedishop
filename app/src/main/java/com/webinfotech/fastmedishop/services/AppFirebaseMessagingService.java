package com.webinfotech.fastmedishop.services;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.os.SystemClock;
import android.util.Log;

import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import com.webinfotech.fastmedishop.AndroidApplication;
import com.webinfotech.fastmedishop.domain.models.NotificationModel;
import com.webinfotech.fastmedishop.domain.models.UserInfo;
import com.webinfotech.fastmedishop.presentation.ui.activities.OrderHistoryActivity;
import com.webinfotech.fastmedishop.presentation.ui.activities.SplashActivity;
import com.webinfotech.fastmedishop.util.NotificationUtils;

public class AppFirebaseMessagingService extends FirebaseMessagingService {

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        String title = "";
        String body = "";
        try {
            title = remoteMessage.getData().get("title");
        } catch (NullPointerException e) {
            title = "Fastmedishop";
        }

        try {
            body = remoteMessage.getData().get("body");
        } catch (NullPointerException e) {
            body = "From Fastmedishop";
        }
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(this);
        if (userInfo != null) {
            Intent resultIntent = new Intent(this, OrderHistoryActivity.class);
            NotificationModel notificationModel = new NotificationModel(title, body);
            NotificationUtils notificationUtils = new NotificationUtils(getApplicationContext());
            notificationUtils.displayNotification(notificationModel, resultIntent);
        }
    }

    @Override
    public void handleIntent(Intent intent) {
        super.handleIntent(intent);
    }

    @Override
    public void onTaskRemoved(Intent rootIntent){
        Intent restartServiceIntent = new Intent(getApplicationContext(), this.getClass());
        restartServiceIntent.setPackage(getPackageName());

        PendingIntent restartServicePendingIntent = PendingIntent.getService(getApplicationContext(), 1, restartServiceIntent, PendingIntent.FLAG_ONE_SHOT);
        AlarmManager alarmService = (AlarmManager) getApplicationContext().getSystemService(Context.ALARM_SERVICE);
        alarmService.set(
                AlarmManager.ELAPSED_REALTIME,
                SystemClock.elapsedRealtime() + 1000,
                restartServicePendingIntent);

        super.onTaskRemoved(rootIntent);
    }

}
