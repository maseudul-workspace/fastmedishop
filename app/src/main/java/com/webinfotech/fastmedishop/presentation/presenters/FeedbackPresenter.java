package com.webinfotech.fastmedishop.presentation.presenters;

public interface FeedbackPresenter {
    void sendFeedback(String message);
    interface View {
        void showLoader();
        void hideLoader();
    }
}
