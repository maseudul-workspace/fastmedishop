package com.webinfotech.fastmedishop.presentation.ui.activities;

import androidx.annotation.LayoutRes;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.drawerlayout.widget.DrawerLayout;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.FrameLayout;

import com.google.android.material.navigation.NavigationView;
import com.webinfotech.fastmedishop.AndroidApplication;
import com.webinfotech.fastmedishop.R;
import com.webinfotech.fastmedishop.domain.models.UserInfo;
import com.webinfotech.fastmedishop.presentation.ui.bottomsheets.LoginBottomSheet;

public class BaseActivity extends AppCompatActivity {

    @BindView(R.id.toolbar) @Nullable
    Toolbar toolbar;
    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    ActionBarDrawerToggle actionBarDrawerToggle;
    @BindView(R.id.navigation)
    NavigationView navigationView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_base);
    }

    protected void inflateContent(@LayoutRes int inflateResID){
        setContentView(R.layout.activity_base);
        FrameLayout contentFramelayout = (FrameLayout) findViewById(R.id.content_frame);
        getLayoutInflater().inflate(inflateResID, contentFramelayout);
        ButterKnife.bind(this);
        setToolbar();
        setUpNavigationView();
    }

    public void setToolbar() {
        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayShowTitleEnabled(false);
        actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.app_name, R.string.app_name);
        drawerLayout.addDrawerListener(actionBarDrawerToggle);
        actionBarDrawerToggle.syncState();
    }

    public void setUpNavigationView() {
        Menu navMenu = navigationView.getMenu();
        if (isLoggedIn()) {
            navMenu.findItem(R.id.nav_log_in).setVisible(false);
        } else {
            navMenu.findItem(R.id.nav_log_out).setVisible(false);
        }
        navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                int id = item.getItemId();
                switch (id) {
                    case R.id.nav_order_medicine:
                        goToOrderMedicineActivity();
                        break;
                    case R.id.nav_shipping_address:
                        if (isLoggedIn()) {
                            onShippingAddressClicked();
                        } else {
                            showLoginBottomSheet();
                        }
                        break;
                    case R.id.nav_order_history:
                        if (isLoggedIn()) {
                            onOrdersClicked();
                        } else {
                            showLoginBottomSheet();
                        }
                        break;
                    case R.id.nav_change_password:
                        if (isLoggedIn()) {
                            onChangePasswordClicked();
                        } else {
                            showLoginBottomSheet();
                        }
                        break;
                    case R.id.nav_profile:
                        if (isLoggedIn()) {
                            onUserProfileClicked();
                        } else {
                            showLoginBottomSheet();
                        }
                        break;
                    case R.id.nav_feedback:
                        goToFeedbackActivity();
                        break;
                    case R.id.nav_chat:
                        goToChatActivity();
                        break;
                    case R.id.nav_log_out:
                        onLogoutClicked();
                        break;
                    case R.id.nav_log_in:
                        showLoginBottomSheet();
                        break;

                }
                return false;
            }
        });
    }

    private void showLoginBottomSheet() {
        LoginBottomSheet loginBottomSheet = new LoginBottomSheet();
        loginBottomSheet.show(getSupportFragmentManager(), "");
    }

    private void goToOrderMedicineActivity() {
        Intent intent = new Intent(this, OrderMedicineActivity.class);
        startActivity(intent);
    }

    void onShippingAddressClicked() {
        Intent intent = new Intent(this, ShippingAddressActivity.class);
        startActivity(intent);
    }

    void onUserProfileClicked() {
        Intent intent = new Intent(this, UserProfileActivity.class);
        startActivity(intent);
    }

    void onChangePasswordClicked() {
        Intent intent = new Intent(this, ChangePasswordActivity.class);
        startActivity(intent);
    }

    void onOrdersClicked() {
        Intent intent = new Intent(this, OrderHistoryActivity.class);
        startActivity(intent);
    }

    private void goToChatActivity() {
        Intent link=new Intent(Intent.ACTION_VIEW, Uri.parse("https://api.whatsapp.com/send?phone=+917086589846"));
        startActivity(link);
    }

    private void goToFeedbackActivity() {
        Intent intent = new Intent(this, FeedbackActivity.class);
        startActivity(intent);
    }

    void onLogoutClicked() {
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        androidApplication.setUserInfo(this, null);
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
        finish();
    }

    private boolean isLoggedIn() {
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(getApplicationContext());
        if (userInfo == null) {
            return false;
        } else {
//            Log.e("LogMsg", "User Id: " + userInfo.userId);
//            Log.e("LogMsg", "Api token: " + userInfo.apiToken);
            return true;
        }
    }

}