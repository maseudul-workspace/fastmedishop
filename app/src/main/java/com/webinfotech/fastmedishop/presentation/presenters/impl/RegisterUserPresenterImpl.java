package com.webinfotech.fastmedishop.presentation.presenters.impl;

import android.content.Context;


import com.webinfotech.fastmedishop.domain.executors.Executor;
import com.webinfotech.fastmedishop.domain.executors.MainThread;
import com.webinfotech.fastmedishop.domain.interactors.RegisterUserInteractor;
import com.webinfotech.fastmedishop.domain.interactors.impl.RegisterUserInteractorImpl;
import com.webinfotech.fastmedishop.presentation.presenters.RegisterUserPresenter;
import com.webinfotech.fastmedishop.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.fastmedishop.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class RegisterUserPresenterImpl extends AbstractPresenter implements RegisterUserPresenter, RegisterUserInteractor.Callback {

    Context mContext;
    RegisterUserPresenter.View mView;
    RegisterUserInteractorImpl registerUserInteractor;

    public RegisterUserPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void registerUser(String name, String email, String mobile, String state, String city, String pin, String address, String password, String confirmPassword) {
        registerUserInteractor = new RegisterUserInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, name, email, mobile, state, city, pin, address, password, confirmPassword);
        registerUserInteractor.execute();
    }

    @Override
    public void onRegisterSuccess() {
        mView.hideLoader();
        Toasty.success(mContext, "Registered Successfully").show();
        mView.onRegisterSuccess();
    }

    @Override
    public void onRegisterFail(String errorMsg) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }
}
