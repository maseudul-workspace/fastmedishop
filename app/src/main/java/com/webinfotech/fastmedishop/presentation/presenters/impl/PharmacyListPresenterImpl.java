package com.webinfotech.fastmedishop.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.fastmedishop.domain.executors.Executor;
import com.webinfotech.fastmedishop.domain.executors.MainThread;
import com.webinfotech.fastmedishop.domain.interactors.FetchPharmacyListInteractor;
import com.webinfotech.fastmedishop.domain.interactors.impl.FetchPharmacyListInteractorImpl;
import com.webinfotech.fastmedishop.domain.models.Pharmacy;
import com.webinfotech.fastmedishop.presentation.presenters.PharmacyListPresenter;
import com.webinfotech.fastmedishop.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.fastmedishop.presentation.ui.adapters.PharmaciesMainAdapter;
import com.webinfotech.fastmedishop.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class PharmacyListPresenterImpl extends AbstractPresenter implements PharmacyListPresenter,
                                                                            FetchPharmacyListInteractor.Callback,
                                                                            PharmaciesMainAdapter.Callback
{

    Context mContext;
    PharmacyListPresenter.View mView;

    public PharmacyListPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchPharmacies() {
        FetchPharmacyListInteractorImpl fetchPharmacyListInteractor = new FetchPharmacyListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this);
        fetchPharmacyListInteractor.execute();
        mView.showLoader();
    }

    @Override
    public void onGettingPharmacyListSuccess(Pharmacy[] pharmacies) {
        PharmaciesMainAdapter adapter = new PharmaciesMainAdapter(mContext, pharmacies, this);
        mView.loadAdapter(adapter);
        mView.hideLoader();
    }

    @Override
    public void onGettingPharmacyListFail(String errorMsg) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg, Toasty.LENGTH_SHORT).show();
    }

    @Override
    public void onOrderClicked(int id, String pharmacyName) {
        mView.onOrderClicked(id, pharmacyName);
    }
}
