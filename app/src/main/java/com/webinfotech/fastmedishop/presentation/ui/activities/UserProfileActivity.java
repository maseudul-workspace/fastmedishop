package com.webinfotech.fastmedishop.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.EditText;

import com.webinfotech.fastmedishop.R;
import com.webinfotech.fastmedishop.domain.executors.impl.ThreadExecutor;
import com.webinfotech.fastmedishop.domain.models.UserInfo;
import com.webinfotech.fastmedishop.presentation.presenters.UserProfilePresenter;
import com.webinfotech.fastmedishop.presentation.presenters.impl.UserProfilePresenterImpl;
import com.webinfotech.fastmedishop.presentation.ui.bottomsheets.LoginBottomSheet;
import com.webinfotech.fastmedishop.threading.MainThreadImpl;

public class UserProfileActivity extends AppCompatActivity implements UserProfilePresenter.View {

    @BindView(R.id.edit_text_name)
    EditText editTextName;
    @BindView(R.id.edit_text_email)
    EditText editTextEmail;
    @BindView(R.id.edit_text_phone)
    EditText editTextPhone;
    @BindView(R.id.edit_text_address)
    EditText editTextAddress;
    @BindView(R.id.edit_text_pincode)
    EditText editTextPin;
    @BindView(R.id.edit_text_city)
    EditText editTextCity;
    @BindView(R.id.edit_text_state)
    EditText editTextState;
    ProgressDialog progressDialog;
    UserProfilePresenterImpl mPresenter;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_user_profile);
        getSupportActionBar().setTitle("User Profile");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        initialisePresenter();
        setUpProgressDialog();
        mPresenter.fetchUserProfile();
    }

    private void initialisePresenter() {
        mPresenter = new UserProfilePresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void loadUserProfileData(UserInfo userInfo) {
        editTextName.setText(userInfo.name);
        editTextAddress.setText(userInfo.address);
        editTextCity.setText(userInfo.city);
        editTextEmail.setText(userInfo.email);
        editTextPhone.setText(userInfo.mobile);
        editTextPin.setText(userInfo.pin);
        editTextState.setText(userInfo.state);
    }

    @Override
    public void showLoginBottomSheet() {
        LoginBottomSheet loginBottomSheet = new LoginBottomSheet();
        loginBottomSheet.show(getSupportFragmentManager(), "");
    }

    @OnClick(R.id.btn_update_profile) void onUpdateProfileClicked() {
        if (editTextName.getText().toString().trim().isEmpty() ||
                editTextEmail.getText().toString().isEmpty() ||
                editTextPhone.getText().toString().trim().isEmpty() ) {
            Toasty.warning(this, "Please fill all the fields").show();
        } else {
            mPresenter.updateProfile(
                    editTextName.getText().toString(),
                    editTextEmail.getText().toString(),
                    editTextPhone.getText().toString(),
                    editTextCity.getText().toString(),
                    editTextState.getText().toString(),
                    editTextPin.getText().toString(),
                    editTextAddress.getText().toString()
            );
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}