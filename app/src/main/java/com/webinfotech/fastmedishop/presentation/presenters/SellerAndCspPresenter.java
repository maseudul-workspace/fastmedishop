package com.webinfotech.fastmedishop.presentation.presenters;

public interface SellerAndCspPresenter {
    void registerCsp(String applicantName, String pharmacyName, String address, String pin, String phoneNo, String email, String phonePe, String bankAC, String ifsc);
    void registerSeller(String pharmacyName, String dlNo, String gstNo, String name, String ownerContactNo, String whatsappNo, String address, String pin, String email, String phonePe, String bankAC, String ifsc);
    void claimIncentive(String cspIdNo, String phoneNo, String fileName);
    interface View {
        void showLoader();
        void hideLoader();
        void showSnackBar(String message);
    }
}
