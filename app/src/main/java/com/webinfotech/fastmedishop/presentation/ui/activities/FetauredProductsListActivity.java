package com.webinfotech.fastmedishop.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;

import com.webinfotech.fastmedishop.R;
import com.webinfotech.fastmedishop.domain.executors.impl.ThreadExecutor;
import com.webinfotech.fastmedishop.presentation.presenters.FeaturedProductsPresenter;
import com.webinfotech.fastmedishop.presentation.presenters.impl.FeaturedProductsPresenterImpl;
import com.webinfotech.fastmedishop.presentation.ui.adapters.ProductsVerticalAdapter;
import com.webinfotech.fastmedishop.presentation.ui.bottomsheets.LoginBottomSheet;
import com.webinfotech.fastmedishop.threading.MainThreadImpl;

public class FetauredProductsListActivity extends AppCompatActivity implements FeaturedProductsPresenter.View {

    @BindView(R.id.recycler_view_featured_products)
    RecyclerView recyclerViewFeaturedProducts;
    FeaturedProductsPresenterImpl mPresenter;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_fetaured_products_list);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Featured Products");
        ButterKnife.bind(this);
        initialisePresenter();
        setUpProgressDialog();
        mPresenter.fetchWishList();
    }

    private void initialisePresenter() {
         mPresenter = new FeaturedProductsPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadAdapter(ProductsVerticalAdapter adapter) {
        recyclerViewFeaturedProducts.setAdapter(adapter);
        recyclerViewFeaturedProducts.setLayoutManager(new GridLayoutManager(this,2));
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void showLoginBottomSheet() {
        LoginBottomSheet loginBottomSheet = new LoginBottomSheet();
        loginBottomSheet.show(getSupportFragmentManager(), "");
    }

    @Override
    public void goToProductDetails(int id) {
        Intent intent = new Intent(this, ProductDetailsActivity.class);
        intent.putExtra("productId", id);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}