package com.webinfotech.fastmedishop.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import android.view.View;

import com.github.chrisbanes.photoview.PhotoView;
import com.webinfotech.fastmedishop.R;
import com.webinfotech.fastmedishop.util.GlideHelper;

import androidx.appcompat.app.AlertDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PhotoviewDialog {

    Context mContext;
    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    @BindView(R.id.photo_view)
    PhotoView photoView;

    public PhotoviewDialog(Context mContext, Activity mActivity) {
        this.mContext = mContext;
        this.mActivity = mActivity;
    }

    public void setUpDialog() {
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.layout_photo_view, null);
        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Material_Light_NoActionBar_Fullscreen);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        ButterKnife.bind(this, dialogContainer);
    }

    public void setPhotoView(String image) {
        GlideHelper.setImageView(mContext, photoView, mContext.getResources().getString(R.string.base_url)  + "files/prescriptiuon/" + image);
    }

    public void showDialog() {
        dialog.show();
    }

    public void hideDialog() {
        dialog.dismiss();
    }

    @OnClick(R.id.img_view_cancel) void onCancelClicked() {
        hideDialog();
    }


}
