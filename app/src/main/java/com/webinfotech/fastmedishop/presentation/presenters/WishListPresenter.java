package com.webinfotech.fastmedishop.presentation.presenters;

import com.webinfotech.fastmedishop.presentation.ui.adapters.WishlistAdapter;

public interface WishListPresenter {
    void fetchWishList();
    interface View {
        void loadAdapter(WishlistAdapter adapter);
        void onProductClicked(int productId);
        void showLoader();
        void hideLoader();
    }
}
