package com.webinfotech.fastmedishop.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.fastmedishop.domain.executors.Executor;
import com.webinfotech.fastmedishop.domain.executors.MainThread;
import com.webinfotech.fastmedishop.domain.interactors.FetchSubcategoriesInteractor;
import com.webinfotech.fastmedishop.domain.interactors.impl.FetchSubcategoriesInteractorImpl;
import com.webinfotech.fastmedishop.domain.models.Subcategory;
import com.webinfotech.fastmedishop.presentation.presenters.SubcategoryPresenter;
import com.webinfotech.fastmedishop.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.fastmedishop.presentation.ui.adapters.SubcategoryAdapter;
import com.webinfotech.fastmedishop.repository.AppRepositoryImpl;

public class SubcategoryPresenterImpl extends AbstractPresenter implements SubcategoryPresenter, FetchSubcategoriesInteractor.Callback, SubcategoryAdapter.Callback {

    Context mContext;
    SubcategoryPresenter.View mView;

    public SubcategoryPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchSubcategories(int categoryId) {
        FetchSubcategoriesInteractorImpl fetchSubcategoriesInteractor = new FetchSubcategoriesInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, categoryId);
        fetchSubcategoriesInteractor.execute();
    }

    @Override
    public void onGettingSubcategoriesSuccess(Subcategory[] subcategories) {
        SubcategoryAdapter subcategoryAdapter = new SubcategoryAdapter(mContext, subcategories, this);
        mView.loadData(subcategoryAdapter);
        mView.hideLoader();
    }

    @Override
    public void onGettingSubcategoriesFail(String errorMsg) {
        mView.hideLoader();
    }

    @Override
    public void onSubcategoryClicked(int subcategoryId) {
        mView.onSubcategoryClicked(subcategoryId);
    }
}
