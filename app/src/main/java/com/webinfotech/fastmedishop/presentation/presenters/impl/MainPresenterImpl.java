package com.webinfotech.fastmedishop.presentation.presenters.impl;

import android.content.Context;
import android.util.Log;

import com.webinfotech.fastmedishop.AndroidApplication;
import com.webinfotech.fastmedishop.domain.executors.Executor;
import com.webinfotech.fastmedishop.domain.executors.MainThread;
import com.webinfotech.fastmedishop.domain.interactors.FetchHomeDataInteractor;
import com.webinfotech.fastmedishop.domain.interactors.UpdateFirebaseTokenInteractor;
import com.webinfotech.fastmedishop.domain.interactors.impl.FetchCartListInteractorImpl;
import com.webinfotech.fastmedishop.domain.interactors.impl.FetchHomeDataInteractorImpl;
import com.webinfotech.fastmedishop.domain.interactors.impl.UpdateFirebaseTokenInteractorImpl;
import com.webinfotech.fastmedishop.domain.models.CartList;
import com.webinfotech.fastmedishop.domain.models.HomeData;
import com.webinfotech.fastmedishop.domain.models.UserInfo;
import com.webinfotech.fastmedishop.presentation.presenters.MainPresenter;
import com.webinfotech.fastmedishop.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.fastmedishop.repository.AppRepositoryImpl;

public class MainPresenterImpl extends AbstractPresenter implements MainPresenter,
                                                                    FetchHomeDataInteractor.Callback,
                                                                    FetchCartListInteractorImpl.Callback,
                                                                    UpdateFirebaseTokenInteractor.Callback

{

    Context mContext;
    MainPresenter.View mView;

    public MainPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchHomeData() {
        FetchHomeDataInteractorImpl fetchHomeDataInteractor = new FetchHomeDataInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl());
        fetchHomeDataInteractor.execute();
    }

    @Override
    public void fetchCartDetails() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            FetchCartListInteractorImpl fetchCartListInteractor = new FetchCartListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.userId);
            fetchCartListInteractor.execute();
        }
    }

    @Override
    public void updateFirebaseToken(String token) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            UpdateFirebaseTokenInteractorImpl updateFirebaseTokenInteractor = new UpdateFirebaseTokenInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl(), userInfo.userId, userInfo.apiToken, token);
            updateFirebaseTokenInteractor.execute();
        }
    }

    @Override
    public void onGettingHomeDataSuccess(HomeData homeData) {
        mView.hideLoader();
        mView.onGettingHomeDataSuccess(homeData);
    }

    @Override
    public void onGettingHomeDataFail(String errorMsg) {
        mView.hideLoader();
    }

    @Override
    public void onGettingCartListSuccess(CartList[] cartLists) {
        mView.loadCartCount(cartLists.length);
    }

    @Override
    public void onGettingCartListFail(String errorMsg, int loginError) {
        mView.loadCartCount(0);
    }

    @Override
    public void onFirebaseTokenUpdateSuccess() {

    }

    @Override
    public void onFirebaseTokenUpdateFail(String errorMsg) {

    }
}
