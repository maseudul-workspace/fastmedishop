package com.webinfotech.fastmedishop.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.fastmedishop.R;
import com.webinfotech.fastmedishop.domain.models.Doctors;
import com.webinfotech.fastmedishop.util.GlideHelper;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class DoctorsAppointmentAdapter extends RecyclerView.Adapter<DoctorsAppointmentAdapter.ViewHolder> {

    public interface Callback {
        void onBookNowClicked();
    }

    Context mContext;
    ArrayList<Doctors> doctors;
    Callback mCallback;

    public DoctorsAppointmentAdapter(Context mContext, ArrayList<Doctors> doctors, Callback mCallback) {
        this.mContext = mContext;
        this.doctors = doctors;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_doctors_appointment, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewItemName.setText(doctors.get(position).name);
        GlideHelper.setImageView(mContext, holder.imgViewItem, doctors.get(position).image);
        holder.btnBookAppointment.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onBookNowClicked();
            }
        });
        holder.txtViewSpeciality.setText(doctors.get(position).specialisation);
    }

    @Override
    public int getItemCount() {
        return doctors.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_item)
        TextView txtViewItemName;
        @BindView(R.id.img_view_item)
        ImageView imgViewItem;
        @BindView(R.id.btn_book_appointment)
        Button btnBookAppointment;
        @BindView(R.id.txt_view_speaciality)
        TextView txtViewSpeciality;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
