package com.webinfotech.fastmedishop.presentation.ui.activities;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.content.Intent;
import android.content.IntentSender;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.google.android.play.core.appupdate.AppUpdateInfo;
import com.google.android.play.core.appupdate.AppUpdateManager;
import com.google.android.play.core.appupdate.AppUpdateManagerFactory;
import com.google.android.play.core.install.model.AppUpdateType;
import com.google.android.play.core.install.model.UpdateAvailability;
import com.google.android.play.core.tasks.OnFailureListener;
import com.google.android.play.core.tasks.Task;
import com.google.firebase.iid.FirebaseInstanceId;
import com.tbuonomo.viewpagerdotsindicator.DotsIndicator;
import com.webinfotech.fastmedishop.AndroidApplication;
import com.webinfotech.fastmedishop.R;
import com.webinfotech.fastmedishop.domain.executors.impl.ThreadExecutor;
import com.webinfotech.fastmedishop.domain.models.Doctors;
import com.webinfotech.fastmedishop.domain.models.HomeData;
import com.webinfotech.fastmedishop.domain.models.Labs;
import com.webinfotech.fastmedishop.domain.models.Subcategory;
import com.webinfotech.fastmedishop.domain.models.UserInfo;
import com.webinfotech.fastmedishop.domain.models.testing.Image;
import com.webinfotech.fastmedishop.domain.models.testing.Product;
import com.webinfotech.fastmedishop.presentation.presenters.MainPresenter;
import com.webinfotech.fastmedishop.presentation.presenters.impl.MainPresenterImpl;
import com.webinfotech.fastmedishop.presentation.ui.adapters.BannersViewpagerAdapter;
import com.webinfotech.fastmedishop.presentation.ui.adapters.DoctorsAppointmentAdapter;
import com.webinfotech.fastmedishop.presentation.ui.adapters.HorizontalItemsAdapter;
import com.webinfotech.fastmedishop.presentation.ui.adapters.LabDiagnosticsAdapter;
import com.webinfotech.fastmedishop.presentation.ui.adapters.MainViewPagerAdapter;
import com.webinfotech.fastmedishop.presentation.ui.adapters.RecentlyViewedProductsAdapter;
import com.webinfotech.fastmedishop.presentation.ui.adapters.SubcategoryAdapter;
import com.webinfotech.fastmedishop.presentation.ui.adapters.TopPharmaciesAdapter;
import com.webinfotech.fastmedishop.presentation.ui.adapters.VerticalItemsAdapter;
import com.webinfotech.fastmedishop.presentation.ui.bottomsheets.LoginBottomSheet;
import com.webinfotech.fastmedishop.threading.MainThreadImpl;

import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;

public class MainActivity extends BaseActivity implements   DoctorsAppointmentAdapter.Callback,
                                                            LabDiagnosticsAdapter.Callback,
                                                            TopPharmaciesAdapter.Callback,
                                                            MainPresenter.View,
                                                            VerticalItemsAdapter.Callback,
                                                            HorizontalItemsAdapter.Callback,
                                                            SubcategoryAdapter.Callback,
                                                            RecentlyViewedProductsAdapter.Callback
{

    @BindView(R.id.viewpager1)
    ViewPager viewPager1;
    @BindView(R.id.viewpager2)
    ViewPager viewPager2;
    @BindView(R.id.viewpager3)
    ViewPager viewPager3;
    @BindView(R.id.bottom_navigation)
    BottomNavigationView bottomNavigationView;
    @BindView(R.id.recycler_view_new_products)
    RecyclerView recyclerViewNewProducts;
    @BindView(R.id.recycler_view_subcategory)
    RecyclerView recyclerViewSubcategory;
    @BindView(R.id.dots_indicator1)
    DotsIndicator dotsIndicator1;
    @BindView(R.id.dots_indicator2)
    DotsIndicator dotsIndicator2;
    @BindView(R.id.recycler_view_recently_viewed_products)
    RecyclerView recyclerViewRecentlyViewedProducts;
    @BindView(R.id.layout_recently_viewed_products)
    View layoutRecentlyViewedProducts;
    @BindView(R.id.recycler_view_popular_products)
    RecyclerView recyclerViewPopularProducts;
    @BindView(R.id.dots_indicator3)
    DotsIndicator dotsIndicator3;
    MainPresenterImpl mPresenter;
    private int currentPage = 0;
    private int currentPage1 = 0;
    private int currentPage2 = 0;
    @BindView(R.id.txt_view_cart_count)
    TextView txtViewCartCount;
    int UPDATE_REQUEST_CODE = 1200;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        inflateContent(R.layout.activity_main);
        ButterKnife.bind(this);
        checkUpdate();
        bottomNavigationView.setItemIconTintList(null);
        setBottomNavigationView();
        initialisePresenter();
        mPresenter.fetchHomeData();
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        mPresenter.updateFirebaseToken(refreshedToken);
    }

    private void setRecyclerViewRecentlyViewedProducts() {
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        ArrayList<Product> products = androidApplication.getRecentlyViewedProducts(this);
        if (products != null && products.size() > 0) {
            layoutRecentlyViewedProducts.setVisibility(View.VISIBLE);
            RecentlyViewedProductsAdapter adapter = new RecentlyViewedProductsAdapter(this, products, this);
            recyclerViewRecentlyViewedProducts.setAdapter(adapter);
            recyclerViewRecentlyViewedProducts.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));
        }
    }

    private void checkUpdate() {
        AppUpdateManager appUpdateManager = AppUpdateManagerFactory.create(this);

        Task<AppUpdateInfo> appUpdateInfoTask = appUpdateManager.getAppUpdateInfo();


        appUpdateInfoTask.addOnSuccessListener(appUpdateInfo -> {
            if (appUpdateInfo.updateAvailability() == UpdateAvailability.UPDATE_AVAILABLE) {
                try {
                    appUpdateManager.startUpdateFlowForResult(appUpdateInfo, AppUpdateType.FLEXIBLE, this, UPDATE_REQUEST_CODE);
                } catch (IntentSender.SendIntentException e) {
                    e.printStackTrace();
                }
            }
        });

        appUpdateInfoTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(Exception e) {
            }
        });

    }


    private void initialisePresenter() {
        mPresenter = new MainPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setBottomNavigationView() {
        bottomNavigationView.setOnNavigationItemSelectedListener(new BottomNavigationView.OnNavigationItemSelectedListener() {
            @Override
            public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                switch (item.getItemId()) {
                    case R.id.nav_my_account:
                       if (isLoggedIn()) {
                           goToUserActivity();
                       } else {
                           showLoginBottomSheet();
                       }
                       break;
                    case R.id.nav_order_medicine:
                        goToOrderMedicineActivity();
                        break;
                }
                return false;
            }
        });
    }

    private boolean isLoggedIn() {
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(getApplicationContext());
        if (userInfo == null) {
            return false;
        } else {
            return true;
        }
    }

    private void showLoginBottomSheet() {
        LoginBottomSheet loginBottomSheet = new LoginBottomSheet();
        loginBottomSheet.show(getSupportFragmentManager(), "");
    }



    private void goToOrderMedicineActivity() {
        Intent intent = new Intent(this, OrderMedicineActivity.class);
        startActivity(intent);
    }

    private void goToMyDoctorActivity() {
        Intent intent = new Intent(this, MyDoctorsActivity.class);
        startActivity(intent);
    }

    private void goToOrderThroughWhatsappActivity() {
        Intent intent = new Intent(this, OrderThroughWhatsappActivity.class);
        startActivity(intent);
    }

    @Override
    public void onBookNowClicked() {
        dialNumber();
    }

    @Override
    public void onBookAppointmentClicked() {
        dialNumber();
    }

    @Override
    public void onOrderClicked(int id, String pharmacy) {
        Intent intent = new Intent(this, OrderMedicineActivity.class);
        intent.putExtra("pharmacyId", id);
        intent.putExtra("pharmacyName", pharmacy);
        startActivity(intent);
    }

    @Override
    public void onGettingHomeDataSuccess(HomeData homeData) {

        HorizontalItemsAdapter newProductsAdapter = new HorizontalItemsAdapter(this, homeData.newProducts, this);
        recyclerViewNewProducts.setAdapter(newProductsAdapter);
        recyclerViewNewProducts.setLayoutManager(new LinearLayoutManager(this,RecyclerView.HORIZONTAL, false));

        HorizontalItemsAdapter popularProductsAdapter = new HorizontalItemsAdapter(this, homeData.popularProducts, this);
        recyclerViewPopularProducts.setAdapter(popularProductsAdapter);
        recyclerViewPopularProducts.setLayoutManager(new LinearLayoutManager(this,RecyclerView.HORIZONTAL, false));

        SubcategoryAdapter subcategoryAdapter = new SubcategoryAdapter(this, homeData.categories[1].subcategories, this);
        recyclerViewSubcategory.setAdapter(subcategoryAdapter);
        recyclerViewSubcategory.setLayoutManager(new GridLayoutManager(this, 3));

        MainViewPagerAdapter adapter1 = new MainViewPagerAdapter(this, homeData.banners.sliders1);
        viewPager1.setAdapter(adapter1);
        dotsIndicator1.setViewPager(viewPager1);

        MainViewPagerAdapter adapter2 = new MainViewPagerAdapter(this, homeData.banners.sliders2);
        viewPager2.setAdapter(adapter2);
        dotsIndicator2.setViewPager(viewPager2);

//        setViewPager1(homeData.banners.sliders1.length);
//        setViewPager2(homeData.banners.sliders2.length);
//
        ArrayList<Image> bannerImages = new ArrayList<>();
        Image bannerImage1 = new Image(1, homeData.banners.banners[0].image);
        Image bannerImage2 = new Image(2, homeData.banners.banners[1].image);
        bannerImages.add(bannerImage1);
        bannerImages.add(bannerImage2);

        BannersViewpagerAdapter bannersViewpagerAdapter = new BannersViewpagerAdapter(this, bannerImages);
        viewPager3.setAdapter(bannersViewpagerAdapter);

        dotsIndicator3.setViewPager(viewPager3);
//
//        setViewPager3(bannerImages.size());

    }

    @Override
    public void loadCartCount(int cartCount) {
        if (cartCount > 0) {
            txtViewCartCount.setText(Integer.toString(cartCount));
            txtViewCartCount.setVisibility(View.VISIBLE);
        } else {
            txtViewCartCount.setVisibility(View.GONE);
        }
    }

    private void setViewPager1(int length) {
        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if(currentPage == length)
                {
                    currentPage = 0;
                }
                viewPager1.setCurrentItem(currentPage, true);
                currentPage++;
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 4000, 5000);
    }

    private void setViewPager2(int length) {
        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if(currentPage1 == length)
                {
                    currentPage1 = 0;
                }
                viewPager2.setCurrentItem(currentPage1, true);
                currentPage1++;
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 4000, 5000);
    }

    private void setViewPager3(int length) {
        // Auto start of viewpager
        final Handler handler = new Handler();
        final Runnable Update = new Runnable() {
            public void run() {
                if(currentPage2 == length)
                {
                    currentPage2 = 0;
                }
                viewPager3.setCurrentItem(currentPage2, true);
                currentPage2++;
            }
        };
        Timer swipeTimer = new Timer();
        swipeTimer.schedule(new TimerTask() {
            @Override
            public void run() {
                handler.post(Update);
            }
        }, 4000, 5000);
    }

    @Override
    public void showLoader() {

    }

    @Override
    public void hideLoader() {

    }

    private void goToUserActivity() {
        Intent intent = new Intent(this, UserActivity.class);
        startActivity(intent);
    }

    @Override
    public void onProductClicked(int itemsId) {
        Intent intent = new Intent(this, ProductDetailsActivity.class);
        intent.putExtra("productId", itemsId);
        startActivity(intent);
    }

    @Override
    public void onCategoryClicked(int categoryId) {
        Intent intent = new Intent(this, ProductListActivity.class);
        intent.putExtra("categoryId", categoryId);
        intent.putExtra("type", 2);
        startActivity(intent);
    }

    private void dialNumber() {
        String phone = "6000913778";
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
        startActivity(intent);
    }

    @OnClick(R.id.layout_search) void onSearchClicked() {
        Intent intent = new Intent(this, SearchActivity.class);
        startActivity(intent);
    }

    @OnClick(R.id.layout_cart) void onImageViewCartClicked() {
        if (isLoggedIn()) {
            goToCartActivity();
        } else {
            showLoginBottomSheet();
        }
    }

    private void goToCartActivity() {
        Intent intent = new Intent(this, CartListActivity.class);
        startActivity(intent);
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.fetchCartDetails();
        setRecyclerViewRecentlyViewedProducts();
    }

    @Override
    public void onSubcategoryClicked(int subcategoryId) {
        Intent intent = new Intent(this, ProductListActivity.class);
        intent.putExtra("categoryId", subcategoryId);
        intent.putExtra("type", 2);
        startActivity(intent);
    }
}