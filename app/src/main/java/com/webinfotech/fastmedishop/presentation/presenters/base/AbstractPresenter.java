package com.webinfotech.fastmedishop.presentation.presenters.base;

import com.webinfotech.fastmedishop.domain.executors.Executor;
import com.webinfotech.fastmedishop.domain.executors.MainThread;

/**
 * Created by Raj on 05-01-2019.
 */

public abstract class AbstractPresenter {
    protected Executor mExecutor;
    protected MainThread mMainThread;
    public AbstractPresenter(Executor executor, MainThread mainThread){
        this.mExecutor = executor;
        this.mMainThread = mainThread;
    }
}
