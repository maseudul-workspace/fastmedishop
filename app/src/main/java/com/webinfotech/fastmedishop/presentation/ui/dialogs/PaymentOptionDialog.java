package com.webinfotech.fastmedishop.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.fastmedishop.R;

import androidx.appcompat.app.AlertDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class PaymentOptionDialog {

    public interface Callback {
        void onSubmitClicked(int paymentType);
    }

    Context mContext;
    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    Callback mCallback;
    @BindView(R.id.txt_view_total_amount)
    TextView txtViewTotalAmount;
    @BindView(R.id.img_view_online_payment_check)
    ImageView imgViewOnlineCheck;
    @BindView(R.id.img_view_cod_check)
    ImageView imgViewCodCheck;
    @BindView(R.id.txt_view_convenient_fee)
    TextView txtViewConvenientFee;
    @BindView(R.id.txt_view_shipping_handling)
    TextView txtViewShippingHandling;
    @BindView(R.id.txt_view_order_total)
    TextView txtViewOrderTotal;
    @BindView(R.id.txt_view_discount)
    TextView txtViewDiscount;
    @BindView(R.id.layout_shipping_handling)
    View viewLayoutShippingHandling;
    double payableAmount;
    double codCharge;
    int paymentType = 1;

    public PaymentOptionDialog(Context mContext, Activity mActivity, Callback mCallback) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        this.mCallback = mCallback;
    }

    public void setUpDialog() {
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.layout_payment_options, null);
        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Material_Light_NoActionBar_Fullscreen);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        ButterKnife.bind(this, dialogContainer);
    }

    @OnClick(R.id.layout_online_payment) void onOnlinePaymentClicked() {
        imgViewCodCheck.setVisibility(View.INVISIBLE);
        imgViewOnlineCheck.setVisibility(View.VISIBLE);
        viewLayoutShippingHandling.setVisibility(View.GONE);
        txtViewOrderTotal.setText("Rs. " + String.format("%.2f", payableAmount));
        paymentType = 2;
    }

    @OnClick(R.id.layout_cod) void onCodPaymentClicked() {
        imgViewCodCheck.setVisibility(View.VISIBLE);
        imgViewOnlineCheck.setVisibility(View.INVISIBLE);
        viewLayoutShippingHandling.setVisibility(View.VISIBLE);
        txtViewOrderTotal.setText("Rs. " +  String.format("%.2f", payableAmount + codCharge));
        txtViewShippingHandling.setText("Rs. " + codCharge);
        paymentType = 1;
    }

    public void showDialog() {
        dialog.show();
    }

    public void hideDialog() {
        dialog.dismiss();
    }

    public void setData(double total, double convenient, double payableAmount, double codCharge, double discount) {
        txtViewTotalAmount.setText("Rs. " + String.format("%.2f", total));
        txtViewConvenientFee.setText("Rs. " + convenient);
        txtViewOrderTotal.setText("Rs. " +  String.format("%.2f", payableAmount + codCharge));
        txtViewDiscount.setText("Rs. " + String.format("%.2f", discount));
        txtViewShippingHandling.setText("Rs. " + codCharge);
        this.payableAmount = payableAmount;
        this.codCharge = codCharge;
    }

    @OnClick(R.id.btn_pay) void onPayClicked() {
        mCallback.onSubmitClicked(paymentType);
    }

    @OnClick(R.id.img_view_cancel) void onCancelClicked() {
        hideDialog();
    }

}
