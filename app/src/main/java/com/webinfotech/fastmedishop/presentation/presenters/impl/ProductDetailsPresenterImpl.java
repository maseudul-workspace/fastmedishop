package com.webinfotech.fastmedishop.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.fastmedishop.AndroidApplication;
import com.webinfotech.fastmedishop.domain.executors.Executor;
import com.webinfotech.fastmedishop.domain.executors.MainThread;
import com.webinfotech.fastmedishop.domain.interactors.AddToCartIntercator;
import com.webinfotech.fastmedishop.domain.interactors.CheckPinInteractor;
import com.webinfotech.fastmedishop.domain.interactors.FetchProductDetailsInteractor;
import com.webinfotech.fastmedishop.domain.interactors.impl.AddToCartInteractorImpl;
import com.webinfotech.fastmedishop.domain.interactors.impl.CheckPinInteractorImpl;
import com.webinfotech.fastmedishop.domain.interactors.impl.FetchCartListInteractorImpl;
import com.webinfotech.fastmedishop.domain.interactors.impl.FetchProductDetailsInteractorImpl;
import com.webinfotech.fastmedishop.domain.models.CartList;
import com.webinfotech.fastmedishop.domain.models.Product;
import com.webinfotech.fastmedishop.domain.models.ProductDetailsData;
import com.webinfotech.fastmedishop.domain.models.UserInfo;
import com.webinfotech.fastmedishop.presentation.presenters.ProductDetailsPresenter;
import com.webinfotech.fastmedishop.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.fastmedishop.presentation.ui.adapters.RelatedProductsAdapter;
import com.webinfotech.fastmedishop.repository.AppRepositoryImpl;

import java.util.ArrayList;

import es.dmoral.toasty.Toasty;

public class ProductDetailsPresenterImpl extends AbstractPresenter implements ProductDetailsPresenter,
                                                                                FetchProductDetailsInteractor.Callback,
                                                                                AddToCartIntercator.Callback,
                                                                                FetchCartListInteractorImpl.Callback,
                                                                                RelatedProductsAdapter.Callback,
                                                                                CheckPinInteractor.Callback

{

    Context mContext;
    ProductDetailsPresenter.View mView;

    public ProductDetailsPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchProductDetails(int productId) {
        FetchProductDetailsInteractorImpl fetchProductDetailsInteractor = new FetchProductDetailsInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, productId);
        fetchProductDetailsInteractor.execute();
    }

    @Override
    public void fetchCartDetails() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            FetchCartListInteractorImpl fetchCartListInteractor = new FetchCartListInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.userId);
            fetchCartListInteractor.execute();
        }
    }

    @Override
    public void checkPin(String pin) {
        CheckPinInteractorImpl checkPinInteractor = new CheckPinInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, pin);
        checkPinInteractor.execute();
        mView.showLoader();
    }

    @Override
    public void addToCart(int productId, String qty, String sizeId) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo != null) {
            AddToCartInteractorImpl addToCartInteractor = new AddToCartInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.userId, productId, qty, sizeId);
            addToCartInteractor.execute();
            mView.showLoader();
        } else {
            mView.showLoginSnackbar();
        }
    }

    @Override
    public void onGettingProductDetailsSuccess(ProductDetailsData productDetailsData) {
        Product[] choosedProducts = productDetailsData.relatedData.choosedProducts;
        Product[] relatedProducts = productDetailsData.relatedData.relatedProducts;
        ArrayList<Product> relatedProductsList = new ArrayList<>();

        for (int i = 0; i < choosedProducts.length; i++) {
            relatedProductsList.add(choosedProducts[i]);
        }

        for (int i = 0; i < relatedProducts.length; i++) {
            relatedProductsList.add(relatedProducts[i]);
        }

        RelatedProductsAdapter adapter = new RelatedProductsAdapter(mContext, relatedProductsList, this);

        mView.loadData(productDetailsData.product, adapter);

        mView.hideLoader();
    }

    @Override
    public void onGettingProductDetailsFail(String errorMsg) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }

    @Override
    public void onAddToCartSuccess() {
        mView.hideLoader();
        mView.showCartSnackbar();
        fetchCartDetails();
    }

    @Override
    public void onAddToCartFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }

    @Override
    public void onGettingCartListSuccess(CartList[] cartLists) {
        mView.loadCartCount(cartLists.length);
    }

    @Override
    public void onGettingCartListFail(String errorMsg, int loginError) {
        mView.loadCartCount(0);
    }

    @Override
    public void onProductClicked(int productId) {
        mView.onProductClicked(productId);
    }

    @Override
    public void onPinCodeAvailable() {
        mView.hideLoader();
        mView.setPinMessage("Delivery available ! Estimated delivery time within 3 - 4 days");
    }

    @Override
    public void onPinCodeNotAvailable(String errorMsg) {
        mView.hideLoader();
        mView.setPinMessage(errorMsg);
    }
}
