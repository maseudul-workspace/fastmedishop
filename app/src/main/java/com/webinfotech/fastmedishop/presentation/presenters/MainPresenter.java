package com.webinfotech.fastmedishop.presentation.presenters;

import com.webinfotech.fastmedishop.domain.models.HomeData;

public interface MainPresenter {
    void fetchHomeData();
    void fetchCartDetails();
    void updateFirebaseToken(String token);
    interface View {
        void onGettingHomeDataSuccess(HomeData homeData);
        void loadCartCount(int cartCount);
        void showLoader();
        void hideLoader();
    }
}
