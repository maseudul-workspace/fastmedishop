package com.webinfotech.fastmedishop.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.fastmedishop.AndroidApplication;
import com.webinfotech.fastmedishop.domain.executors.Executor;
import com.webinfotech.fastmedishop.domain.executors.MainThread;
import com.webinfotech.fastmedishop.domain.interactors.FetchShippingAddressDetailsInteractor;
import com.webinfotech.fastmedishop.domain.interactors.UpdateAddressInteractor;
import com.webinfotech.fastmedishop.domain.interactors.impl.FetchShippingAddressDetailsInteractorImpl;
import com.webinfotech.fastmedishop.domain.interactors.impl.UpdateAddressInteractorImpl;
import com.webinfotech.fastmedishop.domain.models.ShippingAddress;
import com.webinfotech.fastmedishop.domain.models.UserInfo;
import com.webinfotech.fastmedishop.presentation.presenters.EditAddressPresenter;
import com.webinfotech.fastmedishop.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.fastmedishop.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class EditAddressPresenterImpl extends AbstractPresenter implements EditAddressPresenter,
                                                                            FetchShippingAddressDetailsInteractor.Callback,
                                                                            UpdateAddressInteractor.Callback
{

    Context mContext;
    EditAddressPresenter.View mView;
    FetchShippingAddressDetailsInteractorImpl fetchShippingAddressDetailsInteractor;
    UpdateAddressInteractorImpl updateAddressInteractorImpl;

    public EditAddressPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchAddressDetails(int addressId) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        if (user == null) {
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            fetchShippingAddressDetailsInteractor = new FetchShippingAddressDetailsInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId, addressId);
            fetchShippingAddressDetailsInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void updateAddress(int addressId, String name, String email, String mobile, String city, String state, String pin, String address) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        if (user == null) {
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            updateAddressInteractorImpl = new UpdateAddressInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.userId, user.apiToken, addressId, name, email, mobile, state, city, pin, address);
            updateAddressInteractorImpl.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onGettingAddressDetailsSuccess(ShippingAddress shippingAddress) {
        mView.loadAddressDetails(shippingAddress);
        mView.hideLoader();
    }

    @Override
    public void onGettingAddressDetailsFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }

    @Override
    public void onUpdateAddressSuccess() {
        mView.hideLoader();
        mView.onEditAddressSuccess();
        Toasty.success(mContext, "Address Updated Successfully").show();
    }

    @Override
    public void onUpdateAddressFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }
}
