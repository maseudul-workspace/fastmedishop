package com.webinfotech.fastmedishop.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.fastmedishop.domain.executors.Executor;
import com.webinfotech.fastmedishop.domain.executors.MainThread;
import com.webinfotech.fastmedishop.domain.interactors.SendOtpInteractor;
import com.webinfotech.fastmedishop.domain.interactors.impl.SendOtpInteractorImpl;
import com.webinfotech.fastmedishop.presentation.presenters.PhoneNumberVerificationPresenter;
import com.webinfotech.fastmedishop.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.fastmedishop.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class PhoneNumberVerificationPresenterImpl extends AbstractPresenter implements PhoneNumberVerificationPresenter, SendOtpInteractor.Callback{

    Context mContext;
    PhoneNumberVerificationPresenter.View mView;

    public PhoneNumberVerificationPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void sendOtp(String phoneNo) {
        SendOtpInteractorImpl sendOtpInteractor = new SendOtpInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, phoneNo);
        sendOtpInteractor.execute();
    }

    @Override
    public void onSendOtpSuccess(String otp) {
        mView.hideLoader();
        Toasty.success(mContext, "Otp Sent Successfully").show();
        mView.onSendOtpSuccess(otp);
    }

    @Override
    public void onSendOtpFail(String errorMsg) {
        mView.hideLoader();
        Toasty.error(mContext, errorMsg, Toast.LENGTH_SHORT).show();
    }
}
