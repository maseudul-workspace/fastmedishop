package com.webinfotech.fastmedishop.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;

import com.webinfotech.fastmedishop.R;
import com.webinfotech.fastmedishop.domain.executors.impl.ThreadExecutor;
import com.webinfotech.fastmedishop.presentation.presenters.ShippingAddressPresenter;
import com.webinfotech.fastmedishop.presentation.presenters.impl.ShippingAddressPresenterImpl;
import com.webinfotech.fastmedishop.presentation.ui.adapters.ShippingAddressAdapter;
import com.webinfotech.fastmedishop.presentation.ui.bottomsheets.LoginBottomSheet;
import com.webinfotech.fastmedishop.threading.MainThreadImpl;

public class ShippingAddressActivity extends AppCompatActivity implements ShippingAddressPresenter.View {

    @BindView(R.id.recycler_view_address)
    RecyclerView recyclerViewShippingAddress;
    ShippingAddressPresenterImpl mPresenter;
    ProgressDialog progressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_shipping_address);
        getSupportActionBar().setTitle("My Address");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        initialisePresenter();
        setUpProgressDialog();
    }

    private void initialisePresenter() {
        mPresenter = new ShippingAddressPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @Override
    public void loadAdapter(ShippingAddressAdapter adapter) {
        recyclerViewShippingAddress.setVisibility(View.VISIBLE);
        recyclerViewShippingAddress.setLayoutManager(new LinearLayoutManager(this));
        recyclerViewShippingAddress.setAdapter(adapter);
    }

    @Override
    public void goToAddressDetails(int addressId) {
        Intent intent = new Intent(this, EditAddressActivity.class);
        intent.putExtra("addressId", addressId);
        startActivity(intent);
    }

    @Override
    public void onAddressFetchFailed() {
        recyclerViewShippingAddress.setVisibility(View.GONE);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void showLoginBottomSheet() {
        LoginBottomSheet loginBottomSheet = new LoginBottomSheet();
        loginBottomSheet.show(getSupportFragmentManager(), "");
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.fetchShippingAddress();
    }

    @OnClick(R.id.txt_view_add_address) void onAddAddressClicked() {
        Intent intent = new Intent(this, AddShippingAddressActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}