package com.webinfotech.fastmedishop.presentation.ui.adapters;

import android.content.Context;
import android.graphics.Paint;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.fastmedishop.R;
import com.webinfotech.fastmedishop.domain.models.testing.Product;
import com.webinfotech.fastmedishop.util.GlideHelper;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class RecentlyViewedProductsAdapter extends RecyclerView.Adapter<RecentlyViewedProductsAdapter.ViewHolder> {

    public interface Callback {
        void onProductClicked(int itemId);
    }

    Context mContext;
    ArrayList<Product> products;
    Callback mCallback;

    public RecentlyViewedProductsAdapter(Context mContext, ArrayList<Product> products, Callback mCallback) {
        this.mContext = mContext;
        this.products = products;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_related_products, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        GlideHelper.setImageView(mContext, holder.imgViewProduct, mContext.getResources().getString(R.string.base_url) + "images/products/thumb/" + products.get(position).mainImage);
        holder.txtViewProductName.setText(products.get(position).name);
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onProductClicked(products.get(position).id);
            }
        });
        holder.txtViewPrice.setText("₹ " + products.get(position).minPrice);
        holder.txtViewMrp.setText("₹ " + products.get(position).mrp);
        holder.txtViewMrp.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        double discount = ((products.get(position).mrp - products.get(position).minPrice)/products.get(position).mrp)*100;
        holder.txtViewDiscount.setText(String.format("%.1f", discount) + "%");
    }

    @Override
    public int getItemCount() {
        return products.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_product)
        ImageView imgViewProduct;
        @BindView(R.id.txt_view_product_name)
        TextView txtViewProductName;
        @BindView(R.id.txt_view_price)
        TextView txtViewPrice;
        @BindView(R.id.txt_view_mrp)
        TextView txtViewMrp;
        @BindView(R.id.main_layout)
        View mainLayout;
        @BindView(R.id.txt_view_discount)
        TextView txtViewDiscount;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
