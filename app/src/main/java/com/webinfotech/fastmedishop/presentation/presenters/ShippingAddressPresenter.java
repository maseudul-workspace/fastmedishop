package com.webinfotech.fastmedishop.presentation.presenters;

import com.webinfotech.fastmedishop.presentation.ui.adapters.ShippingAddressAdapter;

public interface ShippingAddressPresenter {
    void fetchShippingAddress();
    interface View {
        void loadAdapter(ShippingAddressAdapter adapter);
        void goToAddressDetails(int addressId);
        void onAddressFetchFailed();
        void showLoader();
        void hideLoader();
        void showLoginBottomSheet();
    }
}
