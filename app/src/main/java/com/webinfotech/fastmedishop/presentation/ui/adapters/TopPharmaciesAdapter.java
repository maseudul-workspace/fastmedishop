package com.webinfotech.fastmedishop.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.fastmedishop.R;
import com.webinfotech.fastmedishop.domain.models.Pharmacy;
import com.webinfotech.fastmedishop.util.GlideHelper;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class TopPharmaciesAdapter extends RecyclerView.Adapter<TopPharmaciesAdapter.ViewHolder> {

    public interface Callback {
        void onOrderClicked(int id, String pharmacyName);
    }

    Context mContext;
    Pharmacy[] pharmacies;
    Callback mCallback;

    public TopPharmaciesAdapter(Context mContext, Pharmacy[] pharmacies, Callback mCallback) {
        this.mContext = mContext;
        this.pharmacies = pharmacies;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_top_pharmacies, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewItemName.setText(pharmacies[position].name);
        GlideHelper.setImageViewCustomRoundedCorners(mContext, holder.imgViewItem, mContext.getResources().getString(R.string.base_url) + "/images/pharmacy/" + pharmacies[position].image, 10);
        holder.btnOrder.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onOrderClicked(pharmacies[position].id, pharmacies[position].name);
            }
        });
        holder.txtViewPharmacyAddress.setText(pharmacies[position].locationName);
    }

    @Override
    public int getItemCount() {
        return pharmacies.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_item)
        TextView txtViewItemName;
        @BindView(R.id.img_view_item)
        ImageView imgViewItem;
        @BindView(R.id.btn_order)
        Button btnOrder;
        @BindView(R.id.txt_view_pharmacy_address)
        TextView txtViewPharmacyAddress;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
