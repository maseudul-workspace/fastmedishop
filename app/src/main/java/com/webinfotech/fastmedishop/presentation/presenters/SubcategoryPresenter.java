package com.webinfotech.fastmedishop.presentation.presenters;


import com.webinfotech.fastmedishop.presentation.ui.adapters.SubcategoryAdapter;

public interface SubcategoryPresenter {
    void fetchSubcategories(int categoryId);
    interface View {
        void loadData(SubcategoryAdapter adapter);
        void showLoader();
        void hideLoader();
        void onSubcategoryClicked(int id);
    }
}
