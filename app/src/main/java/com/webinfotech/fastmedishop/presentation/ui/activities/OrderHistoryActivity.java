package com.webinfotech.fastmedishop.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.AbsListView;

import com.webinfotech.fastmedishop.R;
import com.webinfotech.fastmedishop.domain.executors.impl.ThreadExecutor;
import com.webinfotech.fastmedishop.domain.models.OrderPlaceData;
import com.webinfotech.fastmedishop.presentation.presenters.OrderHistoryPresenter;
import com.webinfotech.fastmedishop.presentation.presenters.impl.OrderHistoryPresenterImpl;
import com.webinfotech.fastmedishop.presentation.ui.adapters.OrdersAdapter;
import com.webinfotech.fastmedishop.presentation.ui.bottomsheets.LoginBottomSheet;
import com.webinfotech.fastmedishop.presentation.ui.dialogs.PaymentOptionDialog;
import com.webinfotech.fastmedishop.presentation.ui.dialogs.PhotoviewDialog;
import com.webinfotech.fastmedishop.threading.MainThreadImpl;

import java.util.UUID;

public class OrderHistoryActivity extends AppCompatActivity implements OrderHistoryPresenter.View, PaymentOptionDialog.Callback {

    OrderHistoryPresenterImpl mPresenter;
    ProgressDialog progressDialog;
    Boolean isScrolling = false;
    Integer currentItems;
    Integer totalItems;
    Integer scrollOutItems;
    int pageNo = 1;
    int totalPage = 1;
    @BindView(R.id.recycler_view_order_history)
    RecyclerView recyclerViewOrders;
    LinearLayoutManager layoutManager;
    PhotoviewDialog photoviewDialog;
    PaymentOptionDialog paymentOptionDialog;
    String transactionId;
    String instamojoOrderID;
    String instamojoTransactionID;
    String instamojoPaymentID;
    String instamojoPaymentStatus;
    int orderId;
    String totalAmount;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_history);
        ButterKnife.bind(this);
        initialisePresenter();
        setUpProgressDialog();
        setPhotoviewDialog();
        mPresenter.fetchOrders(pageNo, "refresh");
        getSupportActionBar().setTitle("Order History");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    private void initialisePresenter() {
        mPresenter = new OrderHistoryPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    private void setPhotoviewDialog() {
        photoviewDialog = new PhotoviewDialog(this, this);
        photoviewDialog.setUpDialog();
    }

    @Override
    public void loadAdapter(OrdersAdapter adapter, int totalPage) {
        this.totalPage = totalPage;
        layoutManager = new LinearLayoutManager(this);
        recyclerViewOrders.setLayoutManager(layoutManager);
        recyclerViewOrders.setAdapter(adapter);
        recyclerViewOrders.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
                if(newState == AbsListView.OnScrollListener.SCROLL_STATE_TOUCH_SCROLL)
                {
                    isScrolling= true;
                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                currentItems = layoutManager.getChildCount();
                totalItems  = layoutManager.getItemCount();
                scrollOutItems = layoutManager.findFirstCompletelyVisibleItemPosition();

                if(!recyclerView.canScrollVertically(1))
                {
                    if (pageNo < totalPage) {
                        isScrolling = false;
                        pageNo = pageNo + 1;
                        mPresenter.fetchOrders(pageNo, "");
                    }
                }

            }
        });
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void cancelOrder() {
        isScrolling = true;
        pageNo = 1;
        totalPage = 1;
        mPresenter.fetchOrders(pageNo, "refresh");
    }

    @Override
    public void loadPaymentData(OrderPlaceData orderPlaceData) {
//        totalAmount = Double.toString(orderPlaceData.order.payableAmount);
        transactionId = UUID.randomUUID().toString();
        orderId = orderPlaceData.order.id;
//        mPresenter.initiatePayment(orderPlaceData.paymentData.accessToken.accessToken, Double.toString(orderPlaceData.paymentData.amount), transactionId, mCurrentEnv.name());
    }

    @Override
    public void setOrderId(String id) {

    }

    @Override
    public void onPaymentVerifySuccess() {
        goToPaymentResponseActivity(1);

    }

    @Override
    public void onPaymentVerifyFail() {
        goToPaymentResponseActivity(2);
    }

    @Override
    public void showLoginBottomSheet() {
        LoginBottomSheet loginBottomSheet = new LoginBottomSheet();
        loginBottomSheet.show(getSupportFragmentManager(), "");
    }

    @Override
    public void goToProductDetails(int productId, int productType) {
        Intent intent = new Intent(this, ProductDetailsActivity.class);
        intent.putExtra("productId", productId);
        startActivity(intent);
    }

    @Override
    public void showPrescription(String prescription) {
        photoviewDialog.setPhotoView(prescription);
        photoviewDialog.showDialog();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void onSubmitClicked(int paymentType) {

    }

    private void goToPaymentResponseActivity(int orderStatus) {
        Intent intent = new Intent(this, PaymentResponseActivity.class);
        intent.putExtra("orderId", Integer.toString(orderId));
        intent.putExtra("paymentId", instamojoPaymentID);
        intent.putExtra("orderStatus", orderStatus);
        intent.putExtra("transactionId", instamojoTransactionID);
        intent.putExtra("amount", totalAmount);
        intent.putExtra("orderMsg", "Order placed successfully with anymeds.in and You will be updated once the order is packed and shipped.");
        startActivity(intent);
        finish();
    }

}