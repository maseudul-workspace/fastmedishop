package com.webinfotech.fastmedishop.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.webinfotech.fastmedishop.R;
import com.webinfotech.fastmedishop.domain.models.Size;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SizesAdapter extends RecyclerView.Adapter<SizesAdapter.ViewHolder> {

    public interface Callback {
        void onSizeSelect(int id);
    }

    Context mContext;
    Size[] sizes;
    Callback mCallback;

    public SizesAdapter(Context mContext, Size[] sizes, Callback callback) {
        this.mContext = mContext;
        this.sizes = sizes;
        this.mCallback = callback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_sizes, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewSizeSelected.setText(sizes[position].sizeName);
        holder.txtViewSizeNotSelected.setText(sizes[position].sizeName);
        if (sizes[position].isSelected) {
            holder.txtViewSizeSelected.setVisibility(View.VISIBLE);
            holder.txtViewSizeNotSelected.setVisibility(View.GONE);
        } else {
            holder.txtViewSizeSelected.setVisibility(View.GONE);
            holder.txtViewSizeNotSelected.setVisibility(View.VISIBLE);
        }
        holder.txtViewSizeNotSelected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (sizes[position].stock > 0) {
                    mCallback.onSizeSelect(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return sizes.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_size_selected)
        TextView txtViewSizeSelected;
        @BindView(R.id.txt_view_size_not_selected)
        TextView txtViewSizeNotSelected;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateData(Size[] sizes) {
        this.sizes = sizes;
        notifyDataSetChanged();
    }

}
