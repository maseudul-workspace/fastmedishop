package com.webinfotech.fastmedishop.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.fastmedishop.R;
import com.webinfotech.fastmedishop.domain.models.Labs;
import com.webinfotech.fastmedishop.util.GlideHelper;

import java.util.ArrayList;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class LabDiagnosticsAdapter extends RecyclerView.Adapter<LabDiagnosticsAdapter.ViewHolder> {

    public interface Callback {
        void onBookAppointmentClicked();
    }

    Context mContext;
    ArrayList<Labs> labs;
    Callback mCallback;

    public LabDiagnosticsAdapter(Context mContext, ArrayList<Labs> labs, Callback mCallback) {
        this.mContext = mContext;
        this.labs = labs;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_lab_diagnostics, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.txtViewItemName.setText(labs.get(position).name);
        GlideHelper.setImageView(mContext, holder.imgViewItem, labs.get(position).photo);
        holder.btnContactLab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onBookAppointmentClicked();
            }
        });
        holder.txtViewLabAddress.setText(labs.get(position).address);
    }

    @Override
    public int getItemCount() {
        return labs.size();
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_item)
        TextView txtViewItemName;
        @BindView(R.id.txt_view_lab_address)
        TextView txtViewLabAddress;
        @BindView(R.id.img_view_item)
        ImageView imgViewItem;
        @BindView(R.id.btn_contact_lab)
        Button btnContactLab;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
