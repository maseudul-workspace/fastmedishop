package com.webinfotech.fastmedishop.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.fastmedishop.AndroidApplication;
import com.webinfotech.fastmedishop.domain.executors.Executor;
import com.webinfotech.fastmedishop.domain.executors.MainThread;
import com.webinfotech.fastmedishop.domain.interactors.SendFeedbackInteractor;
import com.webinfotech.fastmedishop.domain.interactors.impl.SendFeedbackInteractorImpl;
import com.webinfotech.fastmedishop.presentation.presenters.FeedbackPresenter;
import com.webinfotech.fastmedishop.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.fastmedishop.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class FeedbackPresenterImpl extends AbstractPresenter implements FeedbackPresenter, SendFeedbackInteractor.Callback {

    Context mContext;
    FeedbackPresenter.View mView;

    public FeedbackPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void sendFeedback(String message) {
        SendFeedbackInteractorImpl sendFeedbackInteractor = new SendFeedbackInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, message);
        sendFeedbackInteractor.execute();
        mView.showLoader();
    }

    @Override
    public void onFeedbackSendSuccess() {
        mView.hideLoader();
        Toasty.success(mContext, "Feedback sent successfully").show();
    }

    @Override
    public void onFeedbackSendFail(String errorMsg) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }
}
