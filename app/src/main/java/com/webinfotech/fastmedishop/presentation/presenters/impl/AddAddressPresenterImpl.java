package com.webinfotech.fastmedishop.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.fastmedishop.AndroidApplication;
import com.webinfotech.fastmedishop.domain.executors.Executor;
import com.webinfotech.fastmedishop.domain.executors.MainThread;
import com.webinfotech.fastmedishop.domain.interactors.AddShippingAddressInteractor;
import com.webinfotech.fastmedishop.domain.interactors.impl.AddShippingAddressInteractorImpl;
import com.webinfotech.fastmedishop.domain.models.UserInfo;
import com.webinfotech.fastmedishop.presentation.presenters.AddAddressPresenter;
import com.webinfotech.fastmedishop.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.fastmedishop.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class AddAddressPresenterImpl extends AbstractPresenter implements AddAddressPresenter, AddShippingAddressInteractor.Callback {

    Context mContext;
    AddAddressPresenter.View mView;
    AddShippingAddressInteractorImpl addShippingAddressInteractor;

    public AddAddressPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void addAddress(String name, String email, String mobile, String city, String state, String pin, String address) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        if (user == null) {
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            addShippingAddressInteractor = new AddShippingAddressInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.userId, user.apiToken, name, email, mobile, state, city, pin, address);
            addShippingAddressInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onAddShippingAddressSuccess() {
        mView.hideLoader();
        Toasty.success(mContext, "Address Added Successfully").show();
        mView.onAddAddressSuccess();
    }

    @Override
    public void onAddShippingAddressFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }
}
