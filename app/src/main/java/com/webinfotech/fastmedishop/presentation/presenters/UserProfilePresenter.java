package com.webinfotech.fastmedishop.presentation.presenters;

import com.webinfotech.fastmedishop.domain.models.UserInfo;

public interface UserProfilePresenter {
    void fetchUserProfile();
    void updateProfile(String name,
                       String email,
                       String mobile,
                       String city,
                       String state,
                       String pin,
                       String address);
    interface View {
        void showLoader();
        void hideLoader();
        void loadUserProfileData(UserInfo userInfo);
        void showLoginBottomSheet();
    }
}
