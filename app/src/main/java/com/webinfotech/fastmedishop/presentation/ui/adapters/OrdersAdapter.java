package com.webinfotech.fastmedishop.presentation.ui.adapters;

import android.content.Context;
import android.content.DialogInterface;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.skydoves.balloon.ArrowOrientation;
import com.skydoves.balloon.Balloon;
import com.skydoves.balloon.BalloonAnimation;
import com.webinfotech.fastmedishop.R;
import com.webinfotech.fastmedishop.domain.models.Orders;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.core.text.HtmlCompat;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class OrdersAdapter extends RecyclerView.Adapter<OrdersAdapter.ViewHolder> implements OrderProductsAdapter.Callback {

    @Override
    public void onProductClicked(int productId) {
        mCallback.onProductClicked(productId);
    }

    public interface Callback {
        void onCancelClicked(int orderId);
        void onProductClicked(int productId);
        void onPrescriptionClicked(String prescription);
        void payAmount(int orderId);
    }

    Context mContext;
    Orders[] orders;
    Callback mCallback;

    public OrdersAdapter(Context mContext, Orders[] orders, Callback mCallback) {
        this.mContext = mContext;
        this.orders = orders;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_orders, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

        holder.txtViewOrderNo.setText("ORDER NO " + orders[position].orderId);
        holder.txtViewTotalAmount.setText("Rs. " + orders[position].totalAmount);
        holder.txtViewPayableAmount.setText("Rs. " + orders[position].payableAmount);

        if (orders[position].payment_status == 1) {
            holder.txtViewPaymentStatus.setText("Pending");
        } else if (orders[position].payment_status == 2) {
            holder.txtViewPaymentStatus.setText("Paid");
        } else {
            holder.txtViewPaymentStatus.setText("Failed");
        }

        switch (orders[position].deliveryStatus) {
            case 1:
                holder.txtViewStatus.setText("Processing");
                holder.txtViewStatus.setTextColor(mContext.getResources().getColor(R.color.md_blue_A400));
                holder.viewInfo.setVisibility(View.GONE);
                break;
            case 2:
                holder.txtViewStatus.setText("Accepted");
                holder.txtViewStatus.setTextColor(mContext.getResources().getColor(R.color.md_teal_A400));
                holder.viewInfo.setVisibility(View.GONE);
                break;
            case 3:
                holder.txtViewStatus.setText("On The Way");
                holder.txtViewStatus.setTextColor(mContext.getResources().getColor(R.color.md_blue_A400));
                holder.viewInfo.setVisibility(View.GONE);
                break;
            case 4:
                holder.txtViewStatus.setText("Delivered");
                holder.txtViewStatus.setTextColor(mContext.getResources().getColor(R.color.md_teal_A400));
                if (orders[position].dispatch_remarks != null) {
                    holder.viewInfo.setVisibility(View.VISIBLE);
                    Balloon balloon = new Balloon.Builder(mContext)
                            .setText(orders[position].dispatch_remarks)
                            .setBalloonAnimation(BalloonAnimation.OVERSHOOT)
                            .setArrowOrientation(ArrowOrientation.BOTTOM)
                            .setArrowColor(mContext.getResources().getColor(R.color.md_blue_A400))
                            .setBackgroundColor(mContext.getResources().getColor(R.color.md_blue_A400))
                            .setMargin(5)
                            .setPadding(10)
                            .setAutoDismissDuration(5000)
                            .build();
                    holder.layoutStatus.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            balloon.showAlignTop(holder.viewInfo);
                        }
                    });
                } else {
                    holder.viewInfo.setVisibility(View.GONE);
                }
                break;
            case 5:
                holder.txtViewStatus.setText("Cancelled");
                holder.txtViewStatus.setTextColor(mContext.getResources().getColor(R.color.md_red_A400));
                holder.viewInfo.setVisibility(View.GONE);
                break;
            case 6:
                holder.txtViewStatus.setText("Delayed");
                holder.txtViewStatus.setTextColor(mContext.getResources().getColor(R.color.md_teal_A400));
                if (orders[position].delay_remarks != null) {
                    holder.viewInfo.setVisibility(View.VISIBLE);
                    Balloon balloon = new Balloon.Builder(mContext)
                            .setText(orders[position].delay_remarks)
                            .setBalloonAnimation(BalloonAnimation.OVERSHOOT)
                            .setArrowOrientation(ArrowOrientation.BOTTOM)
                            .setBackgroundColor(mContext.getResources().getColor(R.color.md_blue_A400))
                            .setArrowColor(mContext.getResources().getColor(R.color.md_blue_A400))
                            .setMargin(5)
                            .setAutoDismissDuration(5000)
                            .build();
                    holder.layoutStatus.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            balloon.showAlignTop(holder.viewInfo);
                        }
                    });
                } else {
                    holder.viewInfo.setVisibility(View.GONE);
                }
                break;
        }

        if (orders[position].deliveryStatus == 1 || orders[position].deliveryStatus == 2) {
            holder.layoutCancel.setVisibility(View.VISIBLE);
            holder.layoutCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    AlertDialog.Builder builder = new AlertDialog.Builder(mContext);
                    builder.setTitle("Confirmation dialog");
                    builder.setMessage("You are about to cancel a order. Do you really want to proceed ?");
                    builder.setCancelable(false);
                    builder.setPositiveButton(Html.fromHtml("<font color='#00BFA5'>Yes</font>"), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            mCallback.onCancelClicked(orders[position].orderId);
                        }
                    });

                    builder.setNegativeButton(Html.fromHtml("<font color='#FF1744'>No</font>"), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {

                        }
                    });

                    builder.show();
                }
            });
        } else {
            holder.layoutCancel.setVisibility(View.GONE);
        }

        OrderProductsAdapter adapter = new OrderProductsAdapter(mContext, orders[position].orderProducts, this);
        holder.recyclerViewOrderProducts.setAdapter(adapter);
        holder.recyclerViewOrderProducts.setLayoutManager(new LinearLayoutManager(mContext));
        holder.txtViewOrderDate.setText("Placed On: " + changeDataFormat(orders[position].date));

        if (orders[position].remarks == null) {
            holder.layoutRemarks.setVisibility(View.GONE);
        } else {
            holder.layoutRemarks.setVisibility(View.VISIBLE);
            holder.txtViewRemarks.setText(HtmlCompat.fromHtml(String.valueOf(HtmlCompat.fromHtml(orders[position].remarks, HtmlCompat.FROM_HTML_MODE_LEGACY)), HtmlCompat.FROM_HTML_MODE_LEGACY), TextView.BufferType.SPANNABLE);
        }

        if (orders[position].is_prescription == 2) {
            holder.layoutPrescription.setVisibility(View.VISIBLE);
        } else {
            holder.layoutPrescription.setVisibility(View.GONE);
        }

        holder.layoutPrescription.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onPrescriptionClicked(orders[position].prescription);
            }
        });

        if (orders[position].discount < 1) {
            holder.txtViewExtraDiscount.setText("No");
        } else {
            holder.txtViewExtraDiscount.setText("Rs. " + orders[position].discount);
        }

    }

    @Override
    public int getItemCount() {
        return orders.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_order_no)
        TextView txtViewOrderNo;
        @BindView(R.id.txt_view_order_date)
        TextView txtViewOrderDate;
        @BindView(R.id.txt_view_total_amount)
        TextView txtViewTotalAmount;
        @BindView(R.id.layout_cancel)
        View layoutCancel;
        @BindView(R.id.txt_view_status)
        TextView txtViewStatus;
        @BindView(R.id.txt_view_payable_amount)
        TextView txtViewPayableAmount;
        @BindView(R.id.txt_view_payment_status)
        TextView txtViewPaymentStatus;
        @BindView(R.id.txt_view_extra_discount)
        TextView txtViewExtraDiscount;
        @BindView(R.id.recycler_view_order_products)
        RecyclerView recyclerViewOrderProducts;
        @BindView(R.id.layout_remarks)
        View layoutRemarks;
        @BindView(R.id.txt_view_remarks)
        TextView txtViewRemarks;
        @BindView(R.id.layout_prescription)
        View layoutPrescription;
        @BindView(R.id.view_info)
        View viewInfo;
        @BindView(R.id.layout_status)
        View layoutStatus;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

    public void updateData(Orders[] orders) {
        this.orders = orders;
        notifyDataSetChanged();
    }

    private String changeDataFormat(String dateString) {
        String inputPattern = "yyyy-MM-dd HH:mm:ss";
        String outputPattern = "EEEE, dd MMM, yyyy";
        SimpleDateFormat inputFormat = new SimpleDateFormat(inputPattern);
        SimpleDateFormat outputFormat = new SimpleDateFormat(outputPattern);

        Date date = null;
        String str = null;

        try {
            date = inputFormat.parse(dateString);
            str = outputFormat.format(date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return str;
    }

}
