package com.webinfotech.fastmedishop.presentation.presenters;

import com.webinfotech.fastmedishop.domain.models.Product;
import com.webinfotech.fastmedishop.presentation.ui.adapters.RelatedProductsAdapter;

public interface ProductDetailsPresenter {
    void fetchProductDetails(int productId);
    void addToCart(int productId, String qty, String sizeId);
    void fetchCartDetails();
    void checkPin(String pin);
    interface View {
        void loadData(Product product, RelatedProductsAdapter relatedProductsAdapter);
        void loadCartCount(int cartCount);
        void onProductClicked(int productId);
        void showLoginSnackbar();
        void showLoader();
        void hideLoader();
        void showCartSnackbar();
        void setPinMessage(String message);
    }
}
