package com.webinfotech.fastmedishop.presentation.presenters.impl;

import android.content.Context;
import android.widget.Toast;

import com.webinfotech.fastmedishop.domain.executors.Executor;
import com.webinfotech.fastmedishop.domain.executors.MainThread;
import com.webinfotech.fastmedishop.domain.interactors.SendForgetPasswordOtpInteractor;
import com.webinfotech.fastmedishop.domain.interactors.impl.SendForgetPasswordOtpInteractorImpl;
import com.webinfotech.fastmedishop.presentation.presenters.ForgetPasswordOtpVerifyPresenter;
import com.webinfotech.fastmedishop.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.fastmedishop.repository.AppRepositoryImpl;


public class ForgetPasswordOtpVerifyPresenterImpl extends AbstractPresenter implements ForgetPasswordOtpVerifyPresenter, SendForgetPasswordOtpInteractor.Callback {

    Context mContext;
    ForgetPasswordOtpVerifyPresenter.View mView;

    public ForgetPasswordOtpVerifyPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void sendOtp(String phone) {
        SendForgetPasswordOtpInteractorImpl sendOtpForgetPasswordInteractor = new SendForgetPasswordOtpInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, phone);
        sendOtpForgetPasswordInteractor.execute();
        mView.showLoader();
    }

    @Override
    public void onSendOtpSuccess(String otp) {
        mView.hideLoader();
        mView.onOtpSendSuccess(otp);
    }

    @Override
    public void onSendOtpFail(String errorMsg) {
        mView.hideLoader();
        Toast.makeText(mContext, errorMsg, Toast.LENGTH_LONG).show();
    }
}
