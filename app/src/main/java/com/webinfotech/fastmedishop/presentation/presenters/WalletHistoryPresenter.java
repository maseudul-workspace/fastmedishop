package com.webinfotech.fastmedishop.presentation.presenters;

import com.webinfotech.fastmedishop.domain.models.WalletDetails;
import com.webinfotech.fastmedishop.presentation.ui.adapters.WalletHistoryAdapter;

public interface WalletHistoryPresenter {
    void fetchWalletHistory();
    interface View {
        void loadAdapter(WalletHistoryAdapter adapter, WalletDetails walletDetails);
        void showLoader();
        void hideLoader();
        void showLoginBottomSheet();
    }
}
