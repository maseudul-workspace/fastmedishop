package com.webinfotech.fastmedishop.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.fastmedishop.domain.executors.Executor;
import com.webinfotech.fastmedishop.domain.executors.MainThread;
import com.webinfotech.fastmedishop.domain.interactors.FetchSearchListInteractor;
import com.webinfotech.fastmedishop.domain.interactors.impl.FetchSearchListInteractorImpl;
import com.webinfotech.fastmedishop.domain.models.Product;
import com.webinfotech.fastmedishop.presentation.presenters.SearchListPresenter;
import com.webinfotech.fastmedishop.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.fastmedishop.presentation.ui.adapters.SearchListAdapter;
import com.webinfotech.fastmedishop.repository.AppRepositoryImpl;

public class SearchListPresenterImpl extends AbstractPresenter implements   SearchListPresenter,
                                                                            FetchSearchListInteractor.Callback,
                                                                            SearchListAdapter.Callback
{

    Context mContext;
    SearchListPresenter.View mView;
    String searchKey;

    public SearchListPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchSearchList(String searchKey) {
        this.searchKey = searchKey;
        FetchSearchListInteractorImpl fetchSearchListInteractor = new FetchSearchListInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl(), searchKey);
        fetchSearchListInteractor.execute();
    }

    @Override
    public void onGettingSearchListSuccess(Product[] products, String searchKey) {
        if (this.searchKey.equals(searchKey)) {
            if (products.length > 0) {
                SearchListAdapter adapter = new SearchListAdapter(mContext, products, this);
                mView.loadAdapter(adapter);
            } else {
                mView.onSearchNotFound(searchKey);
            }
        }
    }

    @Override
    public void onGettingSearchListFail(String errorMsg) {

    }

    @Override
    public void onProductClicked(int productId) {
        mView.onProductClicked(productId);
    }
}
