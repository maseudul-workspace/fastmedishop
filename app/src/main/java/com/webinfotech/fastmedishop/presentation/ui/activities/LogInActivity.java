package com.webinfotech.fastmedishop.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.widget.EditText;

import com.google.android.material.textfield.TextInputLayout;
import com.webinfotech.fastmedishop.R;
import com.webinfotech.fastmedishop.domain.executors.impl.ThreadExecutor;
import com.webinfotech.fastmedishop.presentation.presenters.LoginPresenter;
import com.webinfotech.fastmedishop.presentation.presenters.impl.LoginPresenterImpl;
import com.webinfotech.fastmedishop.threading.MainThreadImpl;

public class LogInActivity extends AppCompatActivity implements LoginPresenter.View {

    @BindView(R.id.edit_text_email)
    EditText editTextEmail;
    @BindView(R.id.edit_text_password)
    EditText editTextPassword;
    @BindView(R.id.txt_input_email_layout)
    TextInputLayout textInputLayoutEmailLayout;
    @BindView(R.id.txt_input_password_layout)
    TextInputLayout textInputLayoutPasswordLayout;
    ProgressDialog progressDialog;
    LoginPresenterImpl mPresenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_log_in);
        getSupportActionBar().setTitle("Log In");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        setUpProgressDialog();
        initialisePresenter();
    }

    private void initialisePresenter() {
        mPresenter = new LoginPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    public void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }

    @OnClick(R.id.btn_login) void onLoginClicked() {
        textInputLayoutEmailLayout.setError("");
        textInputLayoutPasswordLayout.setError("");
        if (editTextEmail.getText().toString().trim().isEmpty()) {
            textInputLayoutEmailLayout.setError("Please Insert Phone No");
        } else if (editTextPassword.getText().toString().trim().isEmpty()) {
            textInputLayoutPasswordLayout.setError("Please Insert Password");
        } else {
            mPresenter.checkLogin(  editTextEmail.getText().toString(),
                    editTextPassword.getText().toString()
            );
            showLoader();
        }
    }

    @OnClick(R.id.register_layout) void onRegisterClicked() {
        Intent intent = new Intent(this, PhoneNumberVerificationActivity.class);
        startActivity(intent);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void goToMainActivity() {
//        Intent intent = new Intent(this, MainActivity.class);
//        startActivity(intent);
        finish();
    }

    @OnClick(R.id.txt_view_forget_password) void onForgetPasswordClicked() {
        Intent intent = new Intent(this, ForgetPasswordOtpActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}