package com.webinfotech.fastmedishop.presentation.presenters;

import com.webinfotech.fastmedishop.domain.models.ShippingAddress;
public interface EditAddressPresenter {
    void fetchAddressDetails(int addressId);
    void updateAddress( int addressId,
                        String name,
                        String email,
                        String mobile,
                        String city,
                        String state,
                        String pin,
                        String address);
    interface View {
        void loadAddressDetails(ShippingAddress address);
        void showLoader();
        void hideLoader();
        void onEditAddressSuccess();
    }
}
