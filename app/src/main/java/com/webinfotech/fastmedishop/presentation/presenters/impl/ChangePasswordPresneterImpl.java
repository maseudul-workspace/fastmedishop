package com.webinfotech.fastmedishop.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.fastmedishop.AndroidApplication;
import com.webinfotech.fastmedishop.domain.executors.Executor;
import com.webinfotech.fastmedishop.domain.executors.MainThread;
import com.webinfotech.fastmedishop.domain.interactors.ChangePasswordInteractor;
import com.webinfotech.fastmedishop.domain.interactors.impl.ChangePasswordInteractorImpl;
import com.webinfotech.fastmedishop.domain.models.UserInfo;
import com.webinfotech.fastmedishop.presentation.presenters.ChangePasswordPresenter;
import com.webinfotech.fastmedishop.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.fastmedishop.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class ChangePasswordPresneterImpl extends AbstractPresenter implements ChangePasswordPresenter,
                                                                                ChangePasswordInteractor.Callback {

    Context mContext;
    ChangePasswordPresenter.View mView;
    ChangePasswordInteractorImpl changePasswordInteractor;

    public ChangePasswordPresneterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void changePassword(String oldPassword, String newPassword) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        if (user == null) {
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            changePasswordInteractor = new ChangePasswordInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId, oldPassword, newPassword);
            changePasswordInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onPasswordChangeSuccess() {
        mView.hideLoader();
        Toasty.success(mContext, "Password Changed Successfully").show();
    }

    @Override
    public void onPasswordChangeFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }
}
