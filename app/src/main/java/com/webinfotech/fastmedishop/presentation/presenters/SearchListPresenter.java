package com.webinfotech.fastmedishop.presentation.presenters;

import com.webinfotech.fastmedishop.presentation.ui.adapters.SearchListAdapter;

public interface SearchListPresenter {
    void fetchSearchList(String searchKey);
    interface View {
        void loadAdapter(SearchListAdapter adapter);
        void onSearchNotFound(String searchKey);
        void onProductClicked(int id);
    }
}
