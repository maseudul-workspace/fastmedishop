package com.webinfotech.fastmedishop.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import com.webinfotech.fastmedishop.R;
import com.webinfotech.fastmedishop.domain.models.CategoryProducts;
import com.webinfotech.fastmedishop.domain.models.Product;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class VerticalItemsAdapter extends RecyclerView.Adapter<VerticalItemsAdapter.ViewHolder> implements HorizontalItemsAdapter.Callback {

    @Override
    public void onProductClicked(int itemId) {
        mCallback.onProductClicked(itemId);
    }

    public interface Callback {
        void onProductClicked(int itemsId);
        void onCategoryClicked(int categoryId);
    }

    Context mContext;
    CategoryProducts[] categoryProducts;
    Callback mCallback;

    public VerticalItemsAdapter(Context mContext, CategoryProducts[] categoryProducts, Callback mCallback) {
        this.mContext = mContext;
        this.categoryProducts = categoryProducts;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_items_vertical, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.txtViewItemName.setText(categoryProducts[position].name);
        Product[] finalProducts;
        Product[] products = categoryProducts[position].products;
        Product[] selectedProducts = categoryProducts[position].selectedProducts;
        int len1 = products.length;
        int len2 = selectedProducts.length;
        finalProducts = new Product[len1 + len2];
        System.arraycopy(products, 0, finalProducts, 0, len1);
        System.arraycopy(selectedProducts, 0, finalProducts, len1, len2);
        HorizontalItemsAdapter adapter = new HorizontalItemsAdapter(mContext, finalProducts, this);
        holder.recyclerViewItems.setAdapter(adapter);
        holder.recyclerViewItems.setLayoutManager(new LinearLayoutManager(mContext, RecyclerView.HORIZONTAL, false));
        holder.layoutViewAll.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onCategoryClicked(categoryProducts[position].id);
            }
        });
    }

    @Override
    public int getItemCount() {
        return categoryProducts.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.txt_view_item_name)
        TextView txtViewItemName;
        @BindView(R.id.btn_view_all)
        Button layoutViewAll;
        @BindView(R.id.recycler_view_horizontal_items)
        RecyclerView recyclerViewItems;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
