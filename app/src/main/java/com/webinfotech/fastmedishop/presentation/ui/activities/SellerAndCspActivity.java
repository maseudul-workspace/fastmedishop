package com.webinfotech.fastmedishop.presentation.ui.activities;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.Settings;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.material.snackbar.Snackbar;
import com.webinfotech.fastmedishop.R;
import com.webinfotech.fastmedishop.domain.executors.impl.ThreadExecutor;
import com.webinfotech.fastmedishop.presentation.presenters.SellerAndCspPresenter;
import com.webinfotech.fastmedishop.presentation.presenters.impl.SellerAndCspPresenterImpl;
import com.webinfotech.fastmedishop.presentation.ui.dialogs.CspRegistrationFormDialog;
import com.webinfotech.fastmedishop.presentation.ui.dialogs.IncentiveClaimFormDialog;
import com.webinfotech.fastmedishop.presentation.ui.dialogs.SellerRegistrationFormDialog;
import com.webinfotech.fastmedishop.threading.MainThreadImpl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.webinfotech.fastmedishop.util.Helper.calculateFileSize;
import static com.webinfotech.fastmedishop.util.Helper.getRealPathFromURI;
import static com.webinfotech.fastmedishop.util.Helper.saveImage;

public class SellerAndCspActivity extends AppCompatActivity implements CspRegistrationFormDialog.Callback, IncentiveClaimFormDialog.Callback, SellerRegistrationFormDialog.Callback, SellerAndCspPresenter.View {

    CspRegistrationFormDialog cspRegistrationFormDialog;
    IncentiveClaimFormDialog incentiveClaimFormDialog;
    SellerRegistrationFormDialog sellerRegistrationFormDialog;
    String[] appPremisions = {
            Manifest.permission.WRITE_EXTERNAL_STORAGE,
            Manifest.permission.READ_EXTERNAL_STORAGE
    };
    private static final int PERMISSIONS_REQUEST_CODE = 1240;
    private static final int REQUEST_PIC = 1000;
    SellerAndCspPresenterImpl mPresenter;
    ProgressDialog progressDialog;
    @BindView(R.id.main_layout)
    View mainLayout;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_seller_and_csp);
        getSupportActionBar().setTitle("Seller and CSP");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ButterKnife.bind(this);
        setUpDialogs();
        initialisePresenter();
        setUpProgressDialog();
    }

    private void initialisePresenter() {
        mPresenter = new SellerAndCspPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);

    }

    private void setUpDialogs() {
        cspRegistrationFormDialog = new CspRegistrationFormDialog(this, this, this);
        cspRegistrationFormDialog.setUpDialog();

        incentiveClaimFormDialog = new IncentiveClaimFormDialog(this, this, this);
        incentiveClaimFormDialog.setUpDialog();

        sellerRegistrationFormDialog = new SellerRegistrationFormDialog(this, this, this);
        sellerRegistrationFormDialog.setUpDialog();
    }

    @Override
    public void submitCspForm(String applicantName, String pharmacyName, String address, String pin, String phoneNo, String email, String phonePe, String bankAC, String ifsc) {
        cspRegistrationFormDialog.hideDialog();
        mPresenter.registerCsp(applicantName, pharmacyName, address, pin, phoneNo, email, phonePe, bankAC, ifsc);
    }

    @Override
    public void submitIncentiveClaimForm(String cspIdNo, String phoneNo, String fileName) {
        incentiveClaimFormDialog.hideDialog();
        mPresenter.claimIncentive(cspIdNo, phoneNo, fileName);
    }

    @Override
    public void onImageClicked() {
        if (checkAndRequestPermissions()) {
            loadImageChooser();
        }
    }

    @Override
    public void submitSellerForm(String pharmacyName, String dlNo, String gstNo, String name, String ownerContactNo, String whatsappNo, String address, String pin, String email, String phonePe, String bankAC, String ifsc) {
        sellerRegistrationFormDialog.hideDialog();
        mPresenter.registerSeller(pharmacyName, dlNo, gstNo, name, ownerContactNo, whatsappNo, address, pin, email, phonePe, bankAC, ifsc);
    }

    @OnClick(R.id.btn_csp_apply) void onCspApplyClicked() {
        cspRegistrationFormDialog.showDialog();
    }

    @OnClick(R.id.btn_seller_registration) void onSellerRegistration() {
        sellerRegistrationFormDialog.showDialog();
    }

    @OnClick(R.id.btn_claim_interactive) void onClaimInteractive() {
        incentiveClaimFormDialog.showDialog();
    }

    private boolean checkAndRequestPermissions() {
        List<String> listPermissionsNeeded = new ArrayList<>();
        for(String perm: appPremisions){
            if(ContextCompat.checkSelfPermission(this, perm) != PackageManager.PERMISSION_GRANTED){
                listPermissionsNeeded.add(perm);
            }
        }
        if(!listPermissionsNeeded.isEmpty()){
            ActivityCompat.requestPermissions(this, listPermissionsNeeded.toArray(new String[listPermissionsNeeded.size()]), PERMISSIONS_REQUEST_CODE);
            return false;
        }
        return true;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == PERMISSIONS_REQUEST_CODE) {
            HashMap<String, Integer> permissionResults = new HashMap<>();
            int deniedCount = 0;
            for (int i = 0; i < grantResults.length; i++) {
                if (grantResults[i] == PackageManager.PERMISSION_DENIED) {
                    permissionResults.put(permissions[i], grantResults[i]);
                    deniedCount++;
                }
            }

            if (deniedCount == 0) {
                loadImageChooser();
            } else {
                for (Map.Entry<String, Integer> entry : permissionResults.entrySet()) {
                    String permName = entry.getKey();
                    int permResult = entry.getValue();
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, permName)) {
                        this.showAlertDialog("", "This app needs read and write storage permissions",
                                "Yes, Grant permissions",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        checkAndRequestPermissions();
                                    }
                                },
                                "Cancel",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Toast.makeText(getApplicationContext(), "Permissions are required ", Toast.LENGTH_SHORT).show();
                                    }
                                },
                                false);
                    } else {
                        this.showAlertDialog("", "You have denied some permissions",
                                "Go to settings",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Intent intent = new Intent(Settings.ACTION_APPLICATION_DETAILS_SETTINGS,
                                                Uri.fromParts("package", getPackageName(), null));
                                        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                                        startActivity(intent);
                                    }
                                },
                                "Cancel",
                                new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialogInterface, int i) {
                                        dialogInterface.dismiss();
                                        Toast.makeText(getApplicationContext(), "Permissions are required", Toast.LENGTH_SHORT).show();
                                    }
                                },
                                false);
                        break;
                    }

                }
            }
        }
    }

    public AlertDialog showAlertDialog(
            String title, String msg, String positiveLabel,
            DialogInterface.OnClickListener positiveOnClick,
            String negativeLabel, DialogInterface.OnClickListener negativeOnClick,
            boolean isCancelable
    ) {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title);
        builder.setCancelable(isCancelable);
        builder.setMessage(msg);
        builder.setPositiveButton(positiveLabel, positiveOnClick);
        builder.setNegativeButton(negativeLabel, negativeOnClick);
        AlertDialog alertDialog = builder.create();
        alertDialog.show();
        return alertDialog;
    }

    private void loadImageChooser() {
        Intent galleryintent = new Intent(Intent.ACTION_PICK);
        galleryintent.setType("image/*");

        Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

        Intent chooser = new Intent(Intent.ACTION_CHOOSER);
        chooser.putExtra(Intent.EXTRA_INTENT, galleryintent);
        chooser.putExtra(Intent.EXTRA_TITLE, "Select from:");

        Intent[] intentArray = { cameraIntent };
        chooser.putExtra(Intent.EXTRA_INITIAL_INTENTS, intentArray);
        startActivityForResult(chooser, REQUEST_PIC);
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // Result code is RESULT_OK only if the user selects an Image
        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode == Activity.RESULT_OK && requestCode == REQUEST_PIC) {
            String filePath;
            if (data.getData() != null) {
                filePath = getRealPathFromURI(data.getData(), this);
                if (calculateFileSize(filePath) > 2) {
                    Toast.makeText(this, "Image should be less than 2 mb", Toast.LENGTH_SHORT).show();
                } else {
                    incentiveClaimFormDialog.setImage(filePath);
                }
            } else {
                Bitmap photo = (Bitmap) data.getExtras().get("data");
                filePath = saveImage(photo);
                if (calculateFileSize(filePath) > 2) {
                    Toast.makeText(this, "Image should be less than 2 mb", Toast.LENGTH_SHORT).show();
                } else {
                    incentiveClaimFormDialog.setImage(filePath);
                }
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void showSnackBar(String message) {
        Snackbar snackbar = Snackbar.make(mainLayout,message,Snackbar.LENGTH_LONG);
        snackbar.setAction("Dismiss", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                snackbar.dismiss();
            }
        });
        snackbar.setBackgroundTint(getResources().getColor(R.color.colorPrimary));
        snackbar.setTextColor(getResources().getColor(R.color.md_white_1000));
        snackbar.setActionTextColor(getResources().getColor(R.color.colorAccent));
        snackbar.setDuration(8000);
        snackbar.show();
    }


}