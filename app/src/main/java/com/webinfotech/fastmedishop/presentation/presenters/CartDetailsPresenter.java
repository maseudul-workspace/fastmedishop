package com.webinfotech.fastmedishop.presentation.presenters;

import com.webinfotech.fastmedishop.domain.models.Charges;
import com.webinfotech.fastmedishop.domain.models.OrderPlaceData;
import com.webinfotech.fastmedishop.presentation.ui.adapters.CartListAdapter;
import com.webinfotech.fastmedishop.presentation.ui.adapters.CartShippingAddressAdapter;

public interface CartDetailsPresenter {
    void fetchCartDetails();
    void fetchShippingAddressList();
    void fetchChargesList();
    void getWalletDetails();
    void placeOrder(int paymentMethod, int addressId, int orderType, int isWallet, String prescription);
    void initiatePayment(String accessToken, String amount, String transactionId, String env);
    void requestPayment(String accessToken, String id);
    void setTransactionId(int orderId, String transactionId);
    void verifyPayment(String instamojoOrderId);
    interface View {
        void showLoader();
        void hideLoader();
        void loadCartAdapter(CartListAdapter adapter, double totalAmount, double discount, double subTotal);
        void loadShippingAddressAdapter(CartShippingAddressAdapter cartShippingAddressAdapter);
        void onShippingAddressEditClicked(int addressId);
        void onAddressSelected(int addressId);
        void onDeliverButtonClicked();
        void loadCharges(Charges[] charges);
        void loadWalletDetails(double walletAmount);
        void showErrorMessage(String errorMsg);
        void hideViews();
        void hideExpressDeliveryOption();
        void changePrescriptionLayoutVisibility(boolean flag);
        void loadPaymentData(OrderPlaceData orderPlaceData);
        void setOrderId(String id);
        void onPaymentVerifySuccess();
        void onPaymentVerifyFail();
        void goToCodPaymentResponse(String amount);
        void showLoginBottomSheet();
        void goToProductDetails(int productId);
    }
}
