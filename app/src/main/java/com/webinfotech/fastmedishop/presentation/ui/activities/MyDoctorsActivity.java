package com.webinfotech.fastmedishop.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;

import com.webinfotech.fastmedishop.R;

public class MyDoctorsActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_doctors);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Search Doctor");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @OnClick(R.id.btn_search) void onSearchDoctorClicked() {
        Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://doctor.anymeds.in/"));
        startActivity(browserIntent);
    }

    @OnClick(R.id.btn_chat) void onWhatsappClicked() {
        Intent link=new Intent(Intent.ACTION_VIEW, Uri.parse("https://api.whatsapp.com/send?phone=+916000913778&text=I want to consult a Doctor"));
        startActivity(link);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}