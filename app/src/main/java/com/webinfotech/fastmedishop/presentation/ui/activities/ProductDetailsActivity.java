package com.webinfotech.fastmedishop.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.text.HtmlCompat;
import androidx.core.widget.NestedScrollView;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.viewpager.widget.ViewPager;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Paint;
import android.os.Bundle;
import android.text.Html;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.android.material.snackbar.Snackbar;
import com.webinfotech.fastmedishop.AndroidApplication;
import com.webinfotech.fastmedishop.R;
import com.webinfotech.fastmedishop.domain.executors.impl.ThreadExecutor;
import com.webinfotech.fastmedishop.domain.models.Product;
import com.webinfotech.fastmedishop.domain.models.Size;
import com.webinfotech.fastmedishop.domain.models.UserInfo;
import com.webinfotech.fastmedishop.presentation.presenters.ProductDetailsPresenter;
import com.webinfotech.fastmedishop.presentation.presenters.impl.ProductDetailsPresenterImpl;
import com.webinfotech.fastmedishop.presentation.ui.adapters.ProductDetailsViewpagerAdapter;
import com.webinfotech.fastmedishop.presentation.ui.adapters.RelatedProductsAdapter;
import com.webinfotech.fastmedishop.presentation.ui.adapters.SizesAdapter;
import com.webinfotech.fastmedishop.presentation.ui.bottomsheets.LoginBottomSheet;
import com.webinfotech.fastmedishop.threading.MainThreadImpl;
import com.webinfotech.fastmedishop.util.GlideHelper;

import java.util.ArrayList;

public class ProductDetailsActivity extends AppCompatActivity implements ProductDetailsPresenter.View, SizesAdapter.Callback, ProductDetailsViewpagerAdapter.Callback {

    @BindView(R.id.recycler_view_sizes)
    RecyclerView recyclerViewSizes;
    @BindView(R.id.view_pager)
    ViewPager viewPager;
    @BindView(R.id.nested_scroll_view)
    NestedScrollView nestedScrollView;
    @BindView(R.id.viewpager_dots_layout)
    LinearLayout dotsIndicatorLayout;
    @BindView(R.id.txt_view_product_name)
    TextView txtViewProductName;
    @BindView(R.id.txt_view_price)
    TextView txtViewProductPrice;
    @BindView(R.id.txt_view_mrp)
    TextView txtViewMrp;
    @BindView(R.id.txt_view_description)
    TextView txtViewDescription;
    @BindView(R.id.recycler_view_related_products)
    RecyclerView recyclerViewRelatedProducts;
    ProductDetailsPresenterImpl mPresenter;
    ProgressDialog progressDialog;
    int productId;
    SizesAdapter sizesAdapter;
    Size[] sizes;
    int minQty;
    int quantity = 1;
    @BindView(R.id.txt_view_qty)
    TextView txtViewQty;
    @BindView(R.id.layout_size)
    View layoutSize;
    @BindView(R.id.main_layout)
    View mainLayout;
    String sizeId = null;
    @BindView(R.id.qty_change_layout)
    View qtyChangeLayout;
    int productType;
    @BindView(R.id.toolbar)
    Toolbar toolbar;
    @BindView(R.id.txt_view_cart_count)
    TextView txtViewCartCount;
    @BindView(R.id.txt_view_discount)
    TextView txtViewDiscount;
    @BindView(R.id.layout_prescription)
    View layoutPrescription;
    Product product;
    @BindView(R.id.img_view_place_holder)
    ImageView imgViewPlaceHolder;
    @BindView(R.id.view_pager_container)
    View viewPagerContainer;
    @BindView(R.id.txt_view_pin_message)
    TextView txtViewPinMessage;
    @BindView(R.id.txt_view_available_pharmacy)
    TextView txtViewAvailablePharmacy;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_product_details);
        productId = getIntent().getIntExtra("productId", 0);
        ButterKnife.bind(this);
        setSupportActionBar(toolbar);
        getSupportActionBar().setTitle(Html.fromHtml("<b>Product Details</b>"));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        initialisePresenter();
        setUpProgressDialog();
        mPresenter.fetchProductDetails(productId);
        showLoader();
    }

    private void initialisePresenter() {
        mPresenter = new ProductDetailsPresenterImpl(ThreadExecutor.getInstance(), MainThreadImpl.getInstance(), this, this);
    }

    private void setUpProgressDialog() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
    }


    private void prepareViewpager1DotsIndicator(int sliderPosition, int length) {
        if(dotsIndicatorLayout.getChildCount() > 0){
            dotsIndicatorLayout.removeAllViews();
        }

        ImageView dots[] = new ImageView[length];
        for(int i = 0; i < length; i++){
            dots[i] = new ImageView(this);
            if(i == sliderPosition){
                GlideHelper.setImageViewWithDrawable(this, dots[i], R.drawable.active_dot);
            }else{
                GlideHelper.setImageViewWithDrawable(this, dots[i], R.drawable.inactive_dot);
            }

            LinearLayout.LayoutParams layoutParams = new LinearLayout.LayoutParams(15, 15);
            layoutParams.setMargins(7, 0, 7, 0);
            dotsIndicatorLayout.addView(dots[i], layoutParams);

        }
    }

    @Override
    public void loadData(Product product, RelatedProductsAdapter relatedProductsAdapter) {
        this.product = product;
        if (product.pharmacyName != null) {
            txtViewAvailablePharmacy.setText("Available in : " + product.pharmacyName);
        }
        txtViewProductName.setText(product.name);
        txtViewDescription.setText(product.description);
        txtViewProductPrice.setText("₹ " + product.minPrice + "/item");
        txtViewMrp.setText("₹ " + product.mrp + "/item");
        txtViewMrp.setPaintFlags(Paint.STRIKE_THRU_TEXT_FLAG);
        double discount = ((product.mrp - product.minPrice)/product.mrp)*100;
        txtViewDiscount.setText(String.format("%.1f", discount) + "% Discount");
        try {
            txtViewDescription.setText(HtmlCompat.fromHtml(String.valueOf(HtmlCompat.fromHtml(product.description, HtmlCompat.FROM_HTML_MODE_COMPACT)), HtmlCompat.FROM_HTML_MODE_COMPACT));
        } catch (NullPointerException e) {

        }

        this.sizes = product.sizes;
        for (int i = 0; i < sizes.length; i++) {
            if (sizes[i].price == product.minPrice) {
                sizes[i].isSelected = true;
                minQty = this.sizes[i].stock;
                sizeId = Integer.toString(this.sizes[i].id);
                break;
            }
        }

        sizesAdapter = new SizesAdapter(this, sizes, this);

        if (product.images.length == 0) {
            imgViewPlaceHolder.setVisibility(View.VISIBLE);
        } else {
            viewPagerContainer.setVisibility(View.VISIBLE);
            ProductDetailsViewpagerAdapter adapter = new ProductDetailsViewpagerAdapter(this, product.images, this);

            viewPager.setAdapter(adapter);

            viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
                @Override
                public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

                }

                @Override
                public void onPageSelected(int position) {
                    prepareViewpager1DotsIndicator(position, product.images.length);
                }

                @Override
                public void onPageScrollStateChanged(int state) {

                }
            });

            prepareViewpager1DotsIndicator(0, product.images.length);
        }


        recyclerViewSizes.setAdapter(sizesAdapter);
        recyclerViewSizes.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));


        recyclerViewRelatedProducts.setAdapter(relatedProductsAdapter);
        recyclerViewRelatedProducts.setLayoutManager(new LinearLayoutManager(this, RecyclerView.HORIZONTAL, false));

        if (product.isPrescription == 2) {
            layoutPrescription.setVisibility(View.VISIBLE);
        }

        setRecentlyViewedProducts();

    }

    private void setRecentlyViewedProducts() {
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        ArrayList<com.webinfotech.fastmedishop.domain.models.testing.Product> products = androidApplication.getRecentlyViewedProducts(this);
        if (products != null && products.size() > 0) {
            int flag = 0;
            for (int i = 0; i < products.size(); i++) {
                if (products.get(i).id == productId) {
                    flag = 1;
                    break;
                }
            }
            if (flag == 0) {
                products.add(new com.webinfotech.fastmedishop.domain.models.testing.Product(product.id, product.name, product.mainImage, product.description, product.pharmacyName, product.minPrice, product.mrp));
            }
        } else {
            products = new ArrayList<>();
            products.add(new com.webinfotech.fastmedishop.domain.models.testing.Product(product.id, product.name, product.mainImage, product.description, product.pharmacyName, product.minPrice, product.mrp));
        }
        androidApplication.setRecentlyViewProducts(this, products);
    }

    @Override
    public void loadCartCount(int cartCount) {
        if (cartCount > 0) {
            txtViewCartCount.setText(Integer.toString(cartCount));
            txtViewCartCount.setVisibility(View.VISIBLE);
        } else {
            txtViewCartCount.setVisibility(View.GONE);
        }
    }

    @Override
    public void onProductClicked(int productId) {
        Intent intent = new Intent(this, ProductDetailsActivity.class);
        intent.putExtra("productId", productId);
        startActivity(intent);
    }

    @Override
    public void showLoader() {
        progressDialog.show();
    }

    @Override
    public void hideLoader() {
        progressDialog.dismiss();
    }

    @Override
    public void showCartSnackbar() {
//        Snackbar snackbar = Snackbar.make(mainLayout,"Successfully Added To Cart",Snackbar.LENGTH_LONG);
//        snackbar.setAction("Go To Cart", new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                goToCartListActivity();
//            }
//        });
//        snackbar.setBackgroundTint(getResources().getColor(R.color.md_green_800));
//        snackbar.setTextColor(getResources().getColor(R.color.md_white_1000));
//        snackbar.setDuration(5000);
//        snackbar.setActionTextColor(getResources().getColor(R.color.md_pink_400));
//        snackbar.show();
        goToCartListActivity();
        
    }

    @Override
    public void setPinMessage(String message) {
        txtViewPinMessage.setText(message);
        View view = this.getCurrentFocus();
        if (view != null) {
            InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
        }
    }

    @Override
    public void onSizeSelect(int position) {
        minQty = sizes[position].stock;
        txtViewProductPrice.setText("₹ " + sizes[position].price + "/item");
        txtViewMrp.setText("₹ " + sizes[position].mrp + "/item");
        for (int i = 0; i < sizes.length; i++) {
            if (i == position) {
                sizes[i].isSelected = true;
            } else {
                sizes[i].isSelected = false;
            }
        }
        sizesAdapter.updateData(sizes);
        sizeId = Integer.toString(sizes[position].id);
    }

    @OnClick(R.id.txt_view_add_qty) void onAddQtyClicked() {
        quantity ++;
        txtViewQty.setText(Integer.toString(quantity));
    }

    @OnClick(R.id.txt_view_minus_qty) void onMinusQtyClicked() {
        if (quantity > 1) {
            quantity --;
            txtViewQty.setText(Integer.toString(quantity));
        } else {
            Toasty.normal(this, "Cant Go Below").show();
        }
    }

    @OnClick(R.id.btn_add_to_cart) void onAddToCartClicked() {
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(this);
        if (userInfo == null) {
            showLoginBottomSheet();
        } else {
            mPresenter.addToCart(productId, Integer.toString(quantity), sizeId);
        }
    }

    @Override
    public void onImageClicked(int id) {

    }

    @Override
    public void showLoginSnackbar() {
        Snackbar snackbar = Snackbar.make(mainLayout,"You must be logged in",Snackbar.LENGTH_LONG);
        snackbar.setAction("Log In", new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent loginIntent = new Intent(getApplicationContext(), LogInActivity.class);
                startActivity(loginIntent);
            }
        });
        snackbar.show();
    }

    @Override
    protected void onResume() {
        super.onResume();
        mPresenter.fetchCartDetails();
    }

    @OnClick(R.id.layout_cart) void onCartClicked() {
        AndroidApplication androidApplication = (AndroidApplication) getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(this);
        if (userInfo == null) {
            showLoginBottomSheet();
        } else {
            goToCartListActivity();
        }
    }

    private void showLoginBottomSheet() {
        LoginBottomSheet loginBottomSheet = new LoginBottomSheet();
        loginBottomSheet.show(getSupportFragmentManager(), "");
    }

    private void goToCartListActivity() {
        Intent intent = new Intent(this, CartListActivity.class);
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}