package com.webinfotech.fastmedishop.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.fastmedishop.AndroidApplication;
import com.webinfotech.fastmedishop.domain.executors.Executor;
import com.webinfotech.fastmedishop.domain.executors.MainThread;
import com.webinfotech.fastmedishop.domain.interactors.FetchShippingAddressInteractor;
import com.webinfotech.fastmedishop.domain.interactors.PlaceOrderInteractor;
import com.webinfotech.fastmedishop.domain.interactors.impl.FetchShippingAddressInteractorImpl;
import com.webinfotech.fastmedishop.domain.interactors.impl.PlaceOrderInteractorImpl;
import com.webinfotech.fastmedishop.domain.models.OrderPlaceData;
import com.webinfotech.fastmedishop.domain.models.ShippingAddress;
import com.webinfotech.fastmedishop.domain.models.UserInfo;
import com.webinfotech.fastmedishop.presentation.presenters.OrderMedicinePresenter;
import com.webinfotech.fastmedishop.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.fastmedishop.presentation.ui.adapters.CartShippingAddressAdapter;
import com.webinfotech.fastmedishop.repository.AppRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class OrderMedicinePresenterImpl extends AbstractPresenter implements    OrderMedicinePresenter,
                                                                                FetchShippingAddressInteractor.Callback,
                                                                                CartShippingAddressAdapter.Callback,
                                                                                PlaceOrderInteractor.Callback
{

    Context mContext;
    OrderMedicinePresenter.View mView;
    FetchShippingAddressInteractorImpl fetchShippingAddressInteractor;
    ShippingAddress[] shippingAddresses;
    CartShippingAddressAdapter cartShippingAddressAdapter;
    int addressId;

    public OrderMedicinePresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchShippingAddressList() {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo userInfo = androidApplication.getUserInfo(mContext);
        if (userInfo == null) {
        } else {
            fetchShippingAddressInteractor = new FetchShippingAddressInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, userInfo.apiToken, userInfo.userId);
            fetchShippingAddressInteractor.execute();
        }
    }

    @Override
    public void placeOrder(String filePath, String pharmacyName, int pharmacyId) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        if (user == null) {
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            PlaceOrderInteractorImpl placeOrderInteractor = new PlaceOrderInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId, 1, addressId, 1, 1, pharmacyId, pharmacyName, filePath);
            placeOrderInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onGettingAddressListSucces(ShippingAddress[] shippingAddresses) {
        this.shippingAddresses = shippingAddresses;
        if (this.shippingAddresses.length > 0) {
            this.shippingAddresses[0].isSelected = true;
        }
        cartShippingAddressAdapter = new CartShippingAddressAdapter(mContext, this.shippingAddresses, this);
        mView.loadShippingAddressAdapter(cartShippingAddressAdapter);
    }

    @Override
    public void onGettingAddressListFail(String errorMsg, int loginError) {

    }

    @Override
    public void onAddressSelected(int id) {
        this.addressId = id;
        for (int i = 0; i < shippingAddresses.length; i++) {
            if (shippingAddresses[i].id == id) {
                shippingAddresses[i].isSelected = true;
            } else {
                shippingAddresses[i].isSelected = false;
            }
        }
        cartShippingAddressAdapter.updateDataSet(this.shippingAddresses);
    }

    @Override
    public void onEditClicked(int id) {
        mView.onShippingAddressEditClicked(id);
    }

    @Override
    public void onDeliverButtonClicked() {
        mView.onDeliverButtonClicked();
    }

    @Override
    public void onPlaceOrderSuccess(OrderPlaceData orderPlaceData) {
        mView.goToOrderHistory("0");
        mView.hideLoader();
    }

    @Override
    public void onPlaceOrderFail(String errorMsg, int loginError) {
        Toasty.warning(mContext, errorMsg, Toasty.LENGTH_SHORT).show();
        mView.hideLoader();
    }
}
