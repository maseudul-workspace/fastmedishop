package com.webinfotech.fastmedishop.presentation.ui.dialogs;

import android.app.Activity;
import android.content.Context;
import android.view.View;
import android.widget.CheckBox;
import android.widget.EditText;

import com.webinfotech.fastmedishop.R;

import androidx.appcompat.app.AlertDialog;
import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import es.dmoral.toasty.Toasty;

public class SellerRegistrationFormDialog {

    public interface Callback {
        void submitSellerForm(String pharmacyName, String dlNo, String gstNo, String name, String ownerContactNo, String whatsappNo, String address, String pin, String email, String phonePe, String bankAC, String ifsc);
    }

    Context mContext;
    View dialogContainer;
    AlertDialog.Builder builder;
    AlertDialog dialog;
    Activity mActivity;
    Callback mCallback;
    @BindView(R.id.edit_text_pharmacy)
    EditText edit_text_pharmacy;
    @BindView(R.id.edit_text_dl_no)
    EditText edit_text_dl_no;
    @BindView(R.id.edit_text_gst_no)
    EditText edit_text_gst_no;
    @BindView(R.id.edit_text_owner_name)
    EditText edit_text_owner_name;
    @BindView(R.id.edit_text_owner_contact_no)
    EditText edit_text_owner_contact_no;
    @BindView(R.id.edit_text_whatsapp_no)
    EditText edit_text_whatsapp_no;
    @BindView(R.id.edit_text_address)
    EditText edit_text_address;
    @BindView(R.id.edit_text_email)
    EditText edit_text_email;
    @BindView(R.id.edit_text_phone_pe)
    EditText edit_text_phone_pe;
    @BindView(R.id.edit_text_bank_ac)
    EditText edit_text_bank_ac;
    @BindView(R.id.edit_text_ifsc)
    EditText edit_text_ifsc;
    @BindView(R.id.check_box_agreement)
    CheckBox checkBoxAgreement;
    @BindView(R.id.edit_text_pin)
    EditText edit_text_pin;

    public SellerRegistrationFormDialog(Context mContext, Activity mActivity, Callback mCallback) {
        this.mContext = mContext;
        this.mActivity = mActivity;
        this.mCallback = mCallback;
    }

    public void setUpDialog() {
        dialogContainer = mActivity.getLayoutInflater().inflate(R.layout.layout_seller_form, null);
        builder = new AlertDialog.Builder(mContext, android.R.style.Theme_Material_Light_NoActionBar_Fullscreen);
        builder.setView(dialogContainer);
        dialog = builder.create();
        dialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        ButterKnife.bind(this, dialogContainer);
    }

    public void showDialog() {
        dialog.show();
    }

    public void hideDialog() {
        dialog.dismiss();
    }

    @OnClick(R.id.img_view_cancel) void onCancelClicked() {
        hideDialog();
    }

    @OnClick(R.id.btn_submit) void onSubmitClicked() {
        if (    edit_text_pharmacy.getText().toString().trim().isEmpty() ||
                edit_text_dl_no.getText().toString().trim().isEmpty() ||
                edit_text_address.getText().toString().trim().isEmpty() ||
                edit_text_gst_no.getText().toString().trim().isEmpty() ||
                edit_text_owner_contact_no.getText().toString().trim().isEmpty() ||
                edit_text_whatsapp_no.getText().toString().trim().isEmpty()

        ) {
            Toasty.warning(mContext, "Some required fields are empty", Toasty.LENGTH_SHORT).show();
        } else if (edit_text_owner_contact_no.getText().toString().trim().length() < 10) {
            Toasty.warning(mContext, "Please enter a 10 digit phone no", Toasty.LENGTH_SHORT).show();
        } else if (edit_text_whatsapp_no.getText().toString().trim().length() < 10) {
            Toasty.warning(mContext, "Please enter a 10 digit Whatsapp no", Toasty.LENGTH_SHORT).show();
        } else if (!checkBoxAgreement.isChecked()) {
            Toasty.warning(mContext, "Please check the terms and condition", Toasty.LENGTH_SHORT).show();
        } else {
            mCallback.submitSellerForm(     edit_text_pharmacy.getText().toString(),
                                            edit_text_dl_no.getText().toString(),
                                            edit_text_gst_no.getText().toString(),
                                            edit_text_owner_name.getText().toString(),
                                            edit_text_owner_contact_no.getText().toString(),
                                            edit_text_whatsapp_no.getText().toString(),
                                            edit_text_address.getText().toString(),
                                            edit_text_pin.getText().toString(),
                                            edit_text_email.getText().toString(),
                                            edit_text_phone_pe.getText().toString(),
                                            edit_text_bank_ac.getText().toString(),
                                            edit_text_ifsc.getText().toString()
            );
        }
    }

}
