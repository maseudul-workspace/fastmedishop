package com.webinfotech.fastmedishop.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.webinfotech.fastmedishop.R;
import com.webinfotech.fastmedishop.domain.models.Subcategory;
import com.webinfotech.fastmedishop.util.GlideHelper;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import butterknife.BindView;
import butterknife.ButterKnife;

public class SubcategoryAdapter extends RecyclerView.Adapter<SubcategoryAdapter.ViewHolder> {

    public interface Callback {
        void onSubcategoryClicked(int subcategoryId);
    }

    Context mContext;
    Subcategory[] subcategories;
    Callback mCallback;

    public SubcategoryAdapter(Context mContext, Subcategory[] subcategories, Callback mCallback) {
        this.mContext = mContext;
        this.subcategories = subcategories;
        this.mCallback = mCallback;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.recycler_view_subcategory, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        GlideHelper.setImageViewCustomRoundedCorners(mContext, holder.imgViewSubcategory, mContext.getResources().getString(R.string.base_url) + "/images/category/sub_category/" + subcategories[position].image, 100);
        holder.txtViewSubcategory.setText(subcategories[position].name);
        holder.mainLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCallback.onSubcategoryClicked(subcategories[position].id);
            }
        });
    }

    @Override
    public int getItemCount() {
        return subcategories.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_view_subcategory)
        ImageView imgViewSubcategory;
        @BindView(R.id.txt_view_subcategory)
        TextView txtViewSubcategory;
        @BindView(R.id.main_layout)
        View mainLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }
    }

}
