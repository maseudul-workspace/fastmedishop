package com.webinfotech.fastmedishop.presentation.presenters.impl;

import android.content.Context;

import com.webinfotech.fastmedishop.AndroidApplication;
import com.webinfotech.fastmedishop.domain.executors.Executor;
import com.webinfotech.fastmedishop.domain.executors.MainThread;
import com.webinfotech.fastmedishop.domain.interactors.CancelOrderInteractor;
import com.webinfotech.fastmedishop.domain.interactors.FetchOrderHistoryInteractor;
import com.webinfotech.fastmedishop.domain.interactors.InitiatePaymentInteractor;
import com.webinfotech.fastmedishop.domain.interactors.PayFromOrderHistoryInteractor;
import com.webinfotech.fastmedishop.domain.interactors.RequestPaymentInteractor;
import com.webinfotech.fastmedishop.domain.interactors.SetTransactionIdInteractor;
import com.webinfotech.fastmedishop.domain.interactors.VerifyPaymentInteractor;
import com.webinfotech.fastmedishop.domain.interactors.impl.CancelOrderInteractorImpl;
import com.webinfotech.fastmedishop.domain.interactors.impl.FetchOrderHistoryInteractorImpl;
import com.webinfotech.fastmedishop.domain.interactors.impl.InitiatePaymentInteractorImpl;
import com.webinfotech.fastmedishop.domain.interactors.impl.PayFromOrderHistoryInteractorImpl;
import com.webinfotech.fastmedishop.domain.interactors.impl.RequestPaymentInteractorImpl;
import com.webinfotech.fastmedishop.domain.interactors.impl.SetTransactionIdInteractorImpl;
import com.webinfotech.fastmedishop.domain.interactors.impl.VerifyPaymentInteractorImpl;
import com.webinfotech.fastmedishop.domain.models.GatewayOrder;
import com.webinfotech.fastmedishop.domain.models.OrderPlaceData;
import com.webinfotech.fastmedishop.domain.models.Orders;
import com.webinfotech.fastmedishop.domain.models.PaymentRequestResponse;
import com.webinfotech.fastmedishop.domain.models.UserInfo;
import com.webinfotech.fastmedishop.presentation.presenters.OrderHistoryPresenter;
import com.webinfotech.fastmedishop.presentation.presenters.base.AbstractPresenter;
import com.webinfotech.fastmedishop.presentation.ui.adapters.OrdersAdapter;
import com.webinfotech.fastmedishop.repository.AppRepositoryImpl;
import com.webinfotech.fastmedishop.repository.PaymentRepositoryImpl;

import es.dmoral.toasty.Toasty;

public class OrderHistoryPresenterImpl extends AbstractPresenter implements OrderHistoryPresenter,
                                                                            FetchOrderHistoryInteractor.Callback,
                                                                            OrdersAdapter.Callback,
                                                                            CancelOrderInteractor.Callback,
                                                                            PayFromOrderHistoryInteractor.Callback,
                                                                            InitiatePaymentInteractor.Callback,
                                                                            RequestPaymentInteractor.Callback,
                                                                            SetTransactionIdInteractor.Callback,
                                                                            VerifyPaymentInteractor.Callback
{
    Context mContext;
    OrderHistoryPresenter.View mView;
    Orders[] newOrders;
    OrdersAdapter adapter;
    int orderId;
    private String accessToken;

    public OrderHistoryPresenterImpl(Executor executor, MainThread mainThread, Context mContext, View mView) {
        super(executor, mainThread);
        this.mContext = mContext;
        this.mView = mView;
    }

    @Override
    public void fetchOrders(int pageNo, String refresh) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        if (user == null) {
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
            mView.showLoginBottomSheet();
        } else {
            if (refresh.equals("refresh")) {
                newOrders = null;
                mView.showLoader();
            }
            FetchOrderHistoryInteractorImpl fetchOrderHistoryInteractor = new FetchOrderHistoryInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId, pageNo);
            fetchOrderHistoryInteractor.execute();
        }
    }

    @Override
    public void initiatePayment(String accessToken, String amount, String transactionId, String env) {
        this.accessToken = accessToken;
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        InitiatePaymentInteractorImpl initiatePaymentInteractor = new InitiatePaymentInteractorImpl(mExecutor, mMainThread, new PaymentRepositoryImpl(), this, accessToken, user.name, "razthemonster@gmail.com", user.mobile, amount, "", "INR", transactionId, "https://test.instamojo.com/integrations/android/redirect/", env);
        initiatePaymentInteractor.execute();
    }

    @Override
    public void requestPayment(String accessToken, String id) {
        RequestPaymentInteractorImpl requestPaymentInteractor = new RequestPaymentInteractorImpl(mExecutor, mMainThread, new PaymentRepositoryImpl(), this, accessToken, id);
        requestPaymentInteractor.execute();
    }

    @Override
    public void setTransactionId(int orderId, String transactionId) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        SetTransactionIdInteractorImpl setTransactionIdInteractor = new SetTransactionIdInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, orderId, transactionId);
        setTransactionIdInteractor.execute();
    }

    @Override
    public void verifyPayment(String instamojoOrderId) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        VerifyPaymentInteractorImpl verifyPaymentInteractor = new VerifyPaymentInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, user.userId, this.orderId, instamojoOrderId);
        verifyPaymentInteractor.execute();
        mView.showLoader();
    }

    @Override
    public void onGettingOrderHistorySuccess(Orders[] orders, int totalPage) {
        Orders[] tempOrders;
        tempOrders = newOrders;
        try {
            int len1 = tempOrders.length;
            int len2 = orders.length;
            newOrders = new Orders[len1 + len2];
            System.arraycopy(tempOrders, 0, newOrders, 0, len1);
            System.arraycopy(orders, 0, newOrders, len1, len2);
            adapter.updateData(newOrders);
            adapter.notifyDataSetChanged();
        }catch (NullPointerException e){
            newOrders = orders;
            adapter = new OrdersAdapter(mContext, orders, this);
            mView.loadAdapter(adapter, totalPage);
        }
        mView.hideLoader();
    }

    @Override
    public void onGettingOrderHistoryFail(String errorMsg, int loginError) {
        mView.hideLoader();
        if (loginError == 1) {
            Toasty.warning(mContext, "Session expired !! Please Login Again").show();
            mView.showLoginBottomSheet();
        } else {
            Toasty.warning(mContext, errorMsg).show();
        }
    }

    @Override
    public void onCancelClicked(int orderId) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        if (user == null) {
            Toasty.warning(mContext, "Your Session Expired!! Please Login Again").show();
        } else {
            CancelOrderInteractorImpl cancelOrderInteractor = new CancelOrderInteractorImpl(mExecutor, mMainThread, new AppRepositoryImpl(), this, user.apiToken, orderId);
            cancelOrderInteractor.execute();
            mView.showLoader();
        }
    }

    @Override
    public void onProductClicked(int productId) {
        mView.goToProductDetails(productId, 0);
    }

    @Override
    public void onPrescriptionClicked(String prescription) {
        mView.showPrescription(prescription);
    }

    @Override
    public void payAmount(int orderId) {
        AndroidApplication androidApplication = (AndroidApplication) mContext.getApplicationContext();
        UserInfo user = androidApplication.getUserInfo(mContext);
        PayFromOrderHistoryInteractorImpl payFromOrderHistoryInteractor = new PayFromOrderHistoryInteractorImpl(mExecutor, mMainThread, this, new AppRepositoryImpl(), user.apiToken, orderId);
        payFromOrderHistoryInteractor.execute();
        mView.showLoader();
    }

    @Override
    public void onOrderCancelSuccess() {
        mView.hideLoader();
        Toasty.success(mContext, "Order Cancelled", Toasty.LENGTH_SHORT).show();
        mView.cancelOrder();
    }

    @Override
    public void onOrderCancelFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.error(mContext, errorMsg, Toasty.LENGTH_SHORT).show();
    }

    @Override
    public void onPlaceOrderSuccess(OrderPlaceData orderPlaceData) {
        this.orderId = orderPlaceData.order.id;
        mView.loadPaymentData(orderPlaceData);
    }

    @Override
    public void onPlaceOrderFail(String errorMsg, int loginError) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }

    @Override
    public void onInitiatePaymentSuccess(GatewayOrder gatewayOrder) {
        requestPayment(accessToken, gatewayOrder.id);
    }

    @Override
    public void onInitiatePaymentFail(String errorMsg) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }

    @Override
    public void onRequestPaymentSuccess(PaymentRequestResponse paymentRequestResponse) {
        setTransactionId(orderId, paymentRequestResponse.orderId);
        mView.setOrderId(paymentRequestResponse.orderId);
        mView.hideLoader();
    }

    @Override
    public void onRequestPaymentFail(String errorMsg) {
        mView.hideLoader();
        Toasty.warning(mContext, errorMsg).show();
    }

    @Override
    public void onSetTransactionIdSuccess() {

    }

    @Override
    public void onSetTransactionIdFail(String errorMsg, int loginError) {

    }

    @Override
    public void onVerifyPaymentSuccess() {
        mView.hideLoader();
        mView.onPaymentVerifySuccess();
    }

    @Override
    public void onVerifyPaymentFail() {
        mView.hideLoader();
        mView.onPaymentVerifyFail();
    }


}
