package com.webinfotech.fastmedishop.presentation.presenters;

public interface ForgetPasswordPresenter {
    void requestPasswordChange(String mobile, String password);
    interface View {
        void onPasswordRequestSuccess();
        void showLoader();
        void hideLoader();
    }
}
