package com.webinfotech.fastmedishop.presentation.ui.activities;

import androidx.appcompat.app.AppCompatActivity;
import butterknife.ButterKnife;
import butterknife.OnClick;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.MenuItem;

import com.webinfotech.fastmedishop.R;

public class OrderThroughWhatsappActivity extends AppCompatActivity {


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_order_through_whatsapp);
        ButterKnife.bind(this);
        getSupportActionBar().setTitle("Order Through Whatsapp");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

    @OnClick(R.id.btn_call) void onCallClicked() {
       dialNumber();
    }

    @OnClick(R.id.btn_chat) void onWhatsappClicked() {
        Intent link=new Intent(Intent.ACTION_VIEW, Uri.parse("https://api.whatsapp.com/send?phone=+916000913778&text=I want to order Medicine/Health product"));
        startActivity(link);
    }

    private void dialNumber() {
        String phone = "6000913778";
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.fromParts("tel", phone, null));
        startActivity(intent);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                super.onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

}