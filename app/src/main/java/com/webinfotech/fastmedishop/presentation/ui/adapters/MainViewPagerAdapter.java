package com.webinfotech.fastmedishop.presentation.ui.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.webinfotech.fastmedishop.R;
import com.webinfotech.fastmedishop.domain.models.Slider;
import com.webinfotech.fastmedishop.util.GlideHelper;

import androidx.annotation.NonNull;
import androidx.viewpager.widget.PagerAdapter;

public class MainViewPagerAdapter extends PagerAdapter {

    Context mContext;
    Slider[] sliders;

    public MainViewPagerAdapter(Context mContext, Slider[] sliders) {
        this.mContext = mContext;
        this.sliders = sliders;
    }

    @Override
    public int getCount() {
        return sliders.length;
    }

    @Override
    public boolean isViewFromObject(@NonNull View view, @NonNull Object object) {
        return view == object;
    }

    @NonNull
    @Override
    public Object instantiateItem(@NonNull ViewGroup container, int position) {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        ViewGroup layout = (ViewGroup) inflater.inflate(R.layout.layout_viewpager_imageview, container, false);
        ImageView imageView = (ImageView) layout.findViewById(R.id.img_view_viewpager);
        GlideHelper.setImageViewCustomRoundedCorners(mContext, imageView, mContext.getResources().getString(R.string.base_url) + "images/slider/" + sliders[position].image, 20);
        container.addView(layout);
        return layout;
    }

    @Override
    public void destroyItem(@NonNull ViewGroup container, int position, @NonNull Object object) {
        container.removeView((View) object);
    }


}
