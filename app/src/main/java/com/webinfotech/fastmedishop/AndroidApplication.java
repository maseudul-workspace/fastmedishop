package com.webinfotech.fastmedishop;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.webinfotech.fastmedishop.domain.models.UserInfo;
import com.webinfotech.fastmedishop.domain.models.testing.Product;

import java.lang.reflect.Type;
import java.util.ArrayList;


public class AndroidApplication extends Application {

    UserInfo userInfo;
    ArrayList<Product> recentlyViewProducts;

    @Override
    public void onCreate() {
        super.onCreate();
    }

    public void setUserInfo(Context context, UserInfo userInfo){
        this.userInfo = userInfo;
        SharedPreferences sharedPref = context.getSharedPreferences(
                "FASTMEDISHOP", Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();

        if(userInfo != null) {
            editor.putString("USER", new Gson().toJson(userInfo));
        } else {
            editor.putString("USER", "");
        }

        editor.commit();
    }

    public UserInfo getUserInfo(Context context){
        UserInfo user;
        if(userInfo != null){
            user = userInfo;
        }else{
            SharedPreferences sharedPref = context.getSharedPreferences(
                    "FASTMEDISHOP", Context.MODE_PRIVATE);
            String userInfoJson = sharedPref.getString("USER","");
            if(userInfoJson.isEmpty()){
                user = null;
            }else{
                try {
                    user = new Gson().fromJson(userInfoJson, UserInfo.class);
                    this.userInfo = user;
                }catch (Exception e){
                    user = null;
                }
            }
        }
        return user;
    }

    public void setRecentlyViewProducts(Context mContext, ArrayList<Product> recentlyViewProducts) {
        this.recentlyViewProducts = recentlyViewProducts;
        SharedPreferences sharedPref = mContext.getSharedPreferences(
                "FASTMEDISHOP", Context.MODE_PRIVATE);

        SharedPreferences.Editor editor = sharedPref.edit();

        if(this.recentlyViewProducts != null) {
            editor.putString("RECENT_PRODUCTS", new Gson().toJson(recentlyViewProducts));
        } else {
            editor.putString("RECENT_PRODUCTS", "");
        }

        editor.commit();
    }

    public ArrayList<Product> getRecentlyViewedProducts(Context mContext) {
        ArrayList<Product> products;
        if(recentlyViewProducts != null){
            products = recentlyViewProducts;
        }else{
            SharedPreferences sharedPref = mContext.getSharedPreferences(
                    "FASTMEDISHOP", Context.MODE_PRIVATE);
            String productsJson = sharedPref.getString("RECENT_PRODUCTS","");
            if(productsJson.isEmpty()){
                products = null;
            }else{
                try {
                    Type listType = new TypeToken<ArrayList<Product>>() {}.getType();
                    products = new Gson().fromJson(productsJson, listType);
                    this.recentlyViewProducts = products;
                }catch (Exception e){
                    products = null;
                }
            }
        }
        return products;
    }

}
